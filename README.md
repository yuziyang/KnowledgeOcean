*********************************更改日期：2018-05-24***********************************************
***************开发环境*******************
1.数据库：5.6.38 MySQL Community Server (GPL)
2.IDE：
         Eclipse Java EE IDE for Web Developers.
         Version: Luna Service Release 1 (4.4.1)
         Build id: 20140925-1800
3.JDK：
         java version "1.8.0_121"
         Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
         Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)
4.服务器：Apache Tomcat/7.0.47

***************使用技术*******************
1.servlet
2.jsp
3.el表达式
4.jstl
5.html+css+js
6.ajax
7.commons-fileupload-1.3.3.jar    文件上传包
8.junit  测试框架

***************特殊目录说明*****************
1.DB.property      数据库配置文件
2.com.yzy97.util   工具包
3.com.yzy97.test   测试包
4.sqlScript        数据库脚本
5.demandAnalysis   需求分析相关文档（以项目实际为准，有部分需求改动或未实现，需求文档没有同步更新）

***************重要说明*******************
1.需要增加一项tomcat的虚拟路径用来处理对于上传文件和之后访问文件处理（在开发过程中，每次打开tomcat都会将上传文件清理，因此使用此虚拟路径）
     具体配置：在tomcat的server.xml最后的context上面增加：
     <Context docPath="URL" path="/bookContext"/>
     其中URL为tomcat的webapp目录

***************需要解决的问题****************
1.书籍文件在线阅读时乱码
           未对读取文件时的编码做任何处理，因此读取文本内容为操作系统默认编码，如果上传文件编码与操作系统默认编码不相同，就会出现乱码
2.在移动端访问项目会出现排版问题
3.在以管理员账户登录，进入个人页面时，上架书籍菜单项有时会需要点击两次出现
4.在使用浏览器自带的回退功能时会出现一些界面显示错误
5.其他暂未发现