package com.yzy97.pojo;

import java.util.List;

/**
 * 分页实体类
 *@version 1.0
 *@author yuziyang
 *@since  2018.04.17
 */
public class PagingPojo {
	/**当前页码*/
	private Integer currentPage;
	/**总页数*/
	private Integer totalPage;
	/**数据总条数*/
	private Integer totalData;
	/**查询到的结果*/
	private List<BookPojo> books;
	
	public Integer getTotalData() {
		return totalData;
	}
	public void setTotalData(Integer totalData) {
		this.totalData = totalData;
	}
	public List<BookPojo> getBooks() {
		return books;
	}
	public void setBooks(List<BookPojo> books) {
		this.books = books;
	}	
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public Integer getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	
	
}
