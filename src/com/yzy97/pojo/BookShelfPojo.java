package com.yzy97.pojo;
/**
 * 书架实体类
 * @version 1.0 
 *@author yuziyang
 *@since  2018.04.17
 */
public class BookShelfPojo {
	/*书籍实体*/
	private BookPojo book;
	/*书籍状态    0：正在阅读   1：收藏     2：已阅读完*/
	private Integer type;
	
	public BookPojo getBook() {
		return book;
	}
	public void setBook(BookPojo book) {
		this.book = book;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
	
}
