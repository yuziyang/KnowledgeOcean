package com.yzy97.pojo;

/**
 * 阅读器的实体类
 * @version 1.0
 *@author yuziyang
 *@since   2018.05.14
 */
public class ReadingPojo {
	/**当前页码*/
	private Integer currentPage;
	/**最终页标志      true：最终页     false：不是最终页*/
	private boolean finalyPage;
	/**当前页的内容*/
	private String content;
	/**书名*/
	private String bookname;
	/**书籍类型*/
	private String type;
	/**书籍文件名*/
	private String fileName;
	
	
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public boolean getFinalyPage() {
		return finalyPage;
	}
	public void setFinalyPage(boolean finalyPage) {
		this.finalyPage = finalyPage;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}
