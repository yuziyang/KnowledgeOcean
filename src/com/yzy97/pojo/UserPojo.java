package com.yzy97.pojo;
/**
 * 用户实体类
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.17
 */
public class UserPojo {
	/*用户名*/
	private String username;
	/*密码*/
	private String password;
	/*用户昵称*/
	private String uNickname;
	/*个性签名*/
	private String uSignature;
	/*手机号*/
	private String uPhone;
	/*邮箱*/
	private String uEmail;
	/*用户权限        0：普通用户   1：管理员*/
	private Integer uPurview;
	
	public String getuNickname() {
		return uNickname;
	}
	public void setuNickname(String uNickname) {
		this.uNickname = uNickname;
	}
	public String getuSignature() {
		return uSignature;
	}
	public void setuSignature(String uSignature) {
		this.uSignature = uSignature;
	}
	public String getuPhone() {
		return uPhone;
	}
	public void setuPhone(String uPhone) {
		this.uPhone = uPhone;
	}
	public String getuEmail() {
		return uEmail;
	}
	public void setuEmail(String uEmail) {
		this.uEmail = uEmail;
	}
	public Integer getuPurview() {
		return uPurview;
	}
	public void setuPurview(Integer uPurview) {
		this.uPurview = uPurview;
	} 
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
