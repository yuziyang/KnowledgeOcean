package com.yzy97.pojo;
/**
 * 书籍实体类
 * @version 1.0   
 *@author yuziyang
 *@since 2018.04.17
 */
public class BookPojo {
	/**书籍编号*/
	private Integer bId;
	/**书籍名*/
	private String bName;
	/**书籍作者*/
	private String bAuthor;
	/**书籍类型*/
	private String bType;
	/**书籍简介*/
	private String bDescription;
	/**书籍封面路径*/
	private String bCoveUrl;
	/**书籍文本路径*/
	private String bTextUrl;
	/**被收藏次数*/
	private Integer collectionTimes;
	
	
	public Integer getbId() {
		return bId;
	}
	public void setbId(Integer bId) {
		this.bId = bId;
	}
	public String getbName() {
		return bName;
	}
	public void setbName(String bName) {
		this.bName = bName;
	}
	public String getbAuthor() {
		return bAuthor;
	}
	public void setbAuthor(String bAuthor) {
		this.bAuthor = bAuthor;
	}
	public String getbType() {
		return bType;
	}
	public void setbType(String bType) {
		this.bType = bType;
	}
	public String getbDescription() {
		return bDescription;
	}
	public void setbDescription(String bDescription) {
		this.bDescription = bDescription;
	}
	public String getbCoveUrl() {
		return bCoveUrl;
	}
	public void setbCoveUrl(String bCoveUrl) {
		this.bCoveUrl = bCoveUrl;
	}
	public String getbTextUrl() {
		return bTextUrl;
	}
	public void setbTextUrl(String bTextUrl) {
		this.bTextUrl = bTextUrl;
	}
	public Integer getCollectionTimes() {
		return collectionTimes;
	}
	public void setCollectionTimes(Integer collectionTimes) {
		this.collectionTimes = collectionTimes;
	}
	
	
}
