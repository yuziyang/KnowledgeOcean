package com.yzy97.services;

import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;

/**
 *    登录服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.23
 */
public class LoginService {
	/**
	 *    登录
	 * @param username   用户名
	 * @param password   密码
	 * @return    根据用户名和密码查询到的昵称
	 * @throws UserException 
	 */
	public String login(String username,String password) throws UserException{
		String nickname = null;
		UserPojoDao upd = new UserPojoDao();
		nickname = upd.usernameAndPasswordToSelect(username, password);
		return nickname;
	}
}
