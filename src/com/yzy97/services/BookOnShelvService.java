package com.yzy97.services;

import com.yzy97.dao.BookPojoDao;
import com.yzy97.exception.BookException;
import com.yzy97.pojo.BookPojo;

/**
 * 书架书籍服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.30
 */
public class BookOnShelvService {
	/**
	 * 插入书籍信息到数据库
	 * @author yuziyang
	 * @param name   书名
	 * @param description   书籍简介
	 * @param author     作者
	 * @param type       书籍类型
	 * @param coveUrl    封面路径
	 * @param textUrl    文本路径
	 * @return    true：成功   false：失败
	 * @throws BookException
	 * @since 2018.04.30
	 */
	public boolean BookOnShelv(String name,String description,String author,String type,String coveUrl,String textUrl) throws BookException{
		BookPojo bp = new BookPojo();
		BookPojoDao bpd = new BookPojoDao();
		bp.setbName(name);
		bp.setbDescription(description);
		bp.setbAuthor(author);
		bp.setbType(type);
		bp.setbCoveUrl(coveUrl);
		bp.setbTextUrl(textUrl);
		boolean bool = bpd.insert(bp);
		return bool;
	}
}
