package com.yzy97.services;

import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;

/**
 * 更新个人信息服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.27
 */
public class UpdateInfoService {
    public boolean update(String oldNickname,UserPojo up) throws UserException{
    	UserPojoDao upd = new UserPojoDao();
    	
    	//1.校验昵称是否重复
    	if(up.getuNickname().equals(oldNickname)){	
    	}else if(upd.nicknameToSelect(up.getuNickname())){
    		throw new UserException("昵称已存在");
    	}
    	
    	return upd.nicknameUpdateOnInfo(oldNickname, up);
    }
    
    
}
