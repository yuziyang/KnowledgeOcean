package com.yzy97.services;

import com.yzy97.dao.BookPojoDao;
import com.yzy97.pojo.PagingPojo;

/**
 * 搜索的服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.20
 */
public class SearchService {
	/**
	 * 根据书名或关键字获得页面信息
	 * @author yuziyang
	 * @param bookname 书名或关键字
	 * @param currentPage 当前页码
	 * @param dataPerPage 每页数据条数
	 * @return   页面信息
	 * @since 2018.05.20
	 */
	public PagingPojo getPageInfoToBookname(String bookname,int currentPage,int dataPerPage){
		PagingPojo pageInfo = new PagingPojo();
		pageInfo.setCurrentPage(currentPage);
		BookPojoDao bpd = new BookPojoDao();
		int totalNum = bpd.getDataNumToBookname(bookname);
		pageInfo.setTotalData(totalNum);
		int totalPage = 0;
		if(totalNum % dataPerPage == 0){
			totalPage = totalNum / dataPerPage;
		}else{
			totalPage = totalNum / dataPerPage + 1;
		}
		pageInfo.setTotalPage(totalPage);
		
		int beginIndex = dataPerPage * (currentPage - 1);
		pageInfo.setBooks(bpd.getBooksToBookname(bookname, beginIndex, dataPerPage));
		return pageInfo;
	}
	
	/**
	 * 根据作者名或关键字获得页面信息
	 * @author yuziyang
	 * @param author 作者名或关键字
	 * @param currentPage 当前页码
	 * @param dataPerPage 每页数据条数
	 * @return   页面信息
	 * @since 2018.05.20
	 */
	public PagingPojo getPageInfoToAuthor(String author,int currentPage,int dataPerPage){
		PagingPojo pageInfo = new PagingPojo();
		pageInfo.setCurrentPage(currentPage);
		BookPojoDao bpd = new BookPojoDao();
		int totalNum = bpd.getDataNumToAuthor(author);
		pageInfo.setTotalData(totalNum);
		int totalPage = 0;
		if(totalNum % dataPerPage == 0){
			totalPage = totalNum / dataPerPage;
		}else{
			totalPage = totalNum / dataPerPage + 1;
		}
		pageInfo.setTotalPage(totalPage);
		
		int beginIndex = dataPerPage * (currentPage - 1);
		int lastIndex = dataPerPage * currentPage;
		pageInfo.setBooks(bpd.getBooksToAuthor(author, beginIndex, lastIndex));
		return pageInfo;
	}
	
	
	
	
	
}
