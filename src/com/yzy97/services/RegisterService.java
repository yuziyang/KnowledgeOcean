package com.yzy97.services;

import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;

/**
 * 注册服务
 * 
 * @version 1.0
 * @author yuziyang
 * @since 2018.04.19
 */
public class RegisterService {

	/**
	 * 注册
	 * 
	 * @author yuziyang
	 * @param nickname
	 *            用户昵称
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @param phone
	 *            手机号
	 * @param email
	 *            邮箱
	 * @return true：注册成功 false：注册失败
	 * @throws UserException 
	 * @since 2018.04.19
	 */
	public boolean register(String nickname, String username, String password,
			String phone, String email) throws UserException {
		UserPojoDao userPojoDao = new UserPojoDao();
		// 数据验证
		// 1.昵称重复性检验
		
		if (userPojoDao.nicknameToSelect(nickname)) {
			String message = "昵称已存在！";
			throw new UserException(message);
		}
		// 2.用户名重复性检验
		if (userPojoDao.usernameToSelect(username)) {
			String message = "用户名已存在！";
			throw new UserException(message);
		}

		// 根据传入数据初始化一个注册用户实体
		UserPojo userPojo = new UserPojo();
		userPojo.setuNickname(nickname);
		userPojo.setUsername(username);
		userPojo.setPassword(password);
		userPojo.setuPhone(phone == null ? "" : phone);
		userPojo.setuEmail(email == null ? "" : email);

		// 将数据录入数据库
		boolean boolInsert = userPojoDao.insert(userPojo);
		//创建书架表
		int id = userPojoDao.usernameToSelectID(username);
		boolean boolCreate = userPojoDao.createTableOnId(id);
		

		if (boolInsert && boolCreate) {
			return true;
		} else {
			return false;
		}
	}
}
