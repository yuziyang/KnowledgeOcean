package com.yzy97.services;

import com.yzy97.dao.BookPojoDao;
import com.yzy97.pojo.PagingPojo;

/**
 * 分类查看服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.04
 */
public class SelectBooksToTypeService {
	/**
	 * 获得分页和书籍信息
	 * @author yuziyang
	 * @param pageNum  当前页码
	 * @param dataPerPage  每页显示的数据条数
	 * @param type    书籍类型
	 * @return 分页信息和书籍信息
	 * @since 2018.05.04
	 */
	public PagingPojo getData(int pageNum,int dataPerPage,String type){
		PagingPojo pp = new PagingPojo();
		BookPojoDao bpd = new BookPojoDao();
		//当前页
		pp.setCurrentPage(pageNum);
		//总共数据条数
		pp.setTotalData(bpd.dataNumToType(type));
		//总页数
		if(pp.getTotalData() % dataPerPage != 0){
			pp.setTotalPage((pp.getTotalData() / dataPerPage) + 1);
		}else {
			pp.setTotalPage((pp.getTotalData() / dataPerPage));
		}
		//书籍信息
		pp.setBooks(bpd.booksToTypeAndPageNum(type, dataPerPage * (pageNum - 1), dataPerPage ));
		return pp;
	}
}
