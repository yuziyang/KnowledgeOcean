package com.yzy97.services;

import java.util.ArrayList;
import java.util.List;

import com.yzy97.dao.BookPojoDao;
import com.yzy97.pojo.PagingPojo;

/**
 * 排行榜查看的服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.17
 */
public class LeaderboardService {
	
	/**
	 * 获得排行榜的页面信息
	 * @author yuziyang
	 * @param currentPage 当前页码
	 * @param dataPerPage 每页数据条数
	 * @return   页面信息
	 * @since 2018.05.17
	 */
	public PagingPojo getPageInfo(int currentPage,int dataPerPage){
		PagingPojo pageInfo = new PagingPojo();
		pageInfo.setCurrentPage(currentPage);
		BookPojoDao bpd = new BookPojoDao();
		int totalNum = bpd.getDataNum();
		pageInfo.setTotalData(totalNum);
		int totalPage = 0;
		if(totalNum % dataPerPage == 0){
			totalPage = totalNum / dataPerPage;
		}else{
			totalPage = totalNum / dataPerPage + 1;
		}
		pageInfo.setTotalPage(totalPage);
		
		int beginIndex = dataPerPage * (currentPage - 1);
		pageInfo.setBooks(bpd.selectBooks(beginIndex, dataPerPage));
		return pageInfo;
	}
}
