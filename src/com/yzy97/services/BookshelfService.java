package com.yzy97.services;

import com.yzy97.dao.BookPojoDao;
import com.yzy97.dao.BookShelfPojoDao;
import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.PagingPojo;
import com.yzy97.pojo.UserPojo;

/**
 * 书架的服务类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.19
 */
public class BookshelfService {
	/**
	 * 获得排行榜的页面信息
	 * @author yuziyang
	 * @param currentPage 当前页码
	 * @param dataPerPage 每页数据条数
	 * @param nickname  用户昵称
	 * @param bsType  书籍的状态，   0：收藏       1：正在读       2：已读完
	 * @return   页面信息
	 * @since 2018.05.19
	 */
	public PagingPojo getPageInfo(int currentPage,int dataPerPage,String nickname,int bsType){
		PagingPojo pageInfo = new PagingPojo();
		pageInfo.setCurrentPage(currentPage);
		UserPojoDao upd = new UserPojoDao();
		BookPojoDao bpd = new BookPojoDao();
		BookShelfPojoDao bspd = new BookShelfPojoDao();
		
		int uId = 0;
		try {
			uId = upd.nicknameToSelectID(nickname);
		} catch (UserException e) {
			
			e.printStackTrace();
		}
		
		int totalNum = bspd.getDataNum(uId,bsType);
		pageInfo.setTotalData(totalNum);
		int totalPage = 0;
		if(totalNum % dataPerPage == 0){
			totalPage = totalNum / dataPerPage;
		}else{
			totalPage = totalNum / dataPerPage + 1;
		}
		pageInfo.setTotalPage(totalPage);
		
		int beginIndex = dataPerPage * (currentPage - 1);
		pageInfo.setBooks(bpd.getBooks(uId, bsType, beginIndex, dataPerPage));
		return pageInfo;
	}
}
