package com.yzy97.services;

import com.yzy97.dao.BookPojoDao;
import com.yzy97.dao.BookShelfPojoDao;
import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.BookException;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.ReadingPojo;

/**
 * 添加书籍到书架的服务
 * 
 * @version 1.0
 * @author yuziyang
 * @since 2018.05.13
 */
public class AddToBookshelfService {
	/**
	 * 添加书籍到书架并增加收藏数
	 * 
	 * @author yuziyang
	 * @param nickname
	 *            用户昵称
	 * @param bookname
	 *            书籍名
	 * @param author
	 *            书籍作者
	 * @param bsType
	 *            书籍的状态， 0：收藏 1：正在读 2：已读完
	 * @return true:成功 false:失败
	 * @throws BookException
	 * @throws UserException
	 * @since 2018.05.13
	 */
	public boolean add(String nickname, String bookname, String author,
			int bsType) throws UserException {
		boolean collectionTimesPlus = false;
		boolean insert = false;
		// 根据昵称获取用户id
		UserPojoDao upd = new UserPojoDao();
		int uId = upd.nicknameToSelectID(nickname);
		// 由书名和作者获取书籍id
		BookPojoDao bpd = new BookPojoDao();
		int bId = bpd.selectBId(bookname, author);
		BookShelfPojoDao bspd = new BookShelfPojoDao();
		// 先查询书架中是否存在书籍
		if (bspd.selectBId(uId, bId)) {
			insert = bspd.updateBsType(uId, bId, bsType);
			if (insert) {
				return true;
			} else {
				return false;
			}
		} else {
			// 不存在，增加收藏数，然后在书架中插入书籍
			collectionTimesPlus = bpd.collectionTimesPlus(bId);
			insert = bspd.insert(uId, bId, bsType);
		}

		if (insert && collectionTimesPlus) {
			return true;
		} else {
			return false;
		}

	}

}
