package com.yzy97.services;

import java.io.File;
import java.util.Map;

import com.sun.org.apache.bcel.internal.generic.NEW;
import com.yzy97.dao.BookPojoDao;
import com.yzy97.pojo.ReadingPojo;
import com.yzy97.util.FileUtil;

/**
 * 阅读的服务
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.14
 */
public class ReadService {
	/**
	 * 在线阅读的服务
	 * @author yuziyang
	 * @param contextPath  tomcat的webapp目录路径
	 * @param bookname  书名
	 * @param author    作者名
	 * @param currentPage  当前页码
	 * @return  在线阅读实体
	 * @since   2018.05.16
	 */
	public  ReadingPojo onlineRead(String contextPath,String bookname,String author,int currentPage){
		ReadingPojo rp = new ReadingPojo();
		BookPojoDao bpd = new BookPojoDao();
		FileUtil fu = FileUtil.getFileUtil(); 
		Map filenameAndType = bpd.getFilenaemAndType(bookname, author);
		String fileName = (String)filenameAndType.get("fileName");
		String type = (String)filenameAndType.get("type");
		String url = contextPath + File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + fileName;
		rp = fu.readFile(url,bookname,type, fileName, currentPage);
		return rp;
	}
	
	/**
	 * 在线阅读的服务
	 * @author yuziyang
	 * @param contextPath   tomcat的webapp目录路径
	 * @param bookname      书名
	 * @param type          类型
	 * @param fileName      文件名
	 * @param currentPage   当前页码
	 * @return      在线阅读实体
	 * @since   2018.05.16  
	 */
	public  ReadingPojo onlineRead(String contextPath,String bookname,String type,String fileName,int currentPage){
		ReadingPojo rp = new ReadingPojo();
		FileUtil fu = FileUtil.getFileUtil(); 
		String url = contextPath + File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + fileName;
		rp = fu.readFile(url,bookname,type, fileName, currentPage);
		return rp;
	}
}
