package com.yzy97.services;

import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;

/**
 * 查询个人信息服务
 * @version 1.0 
 *@author yuziyang
 *@since 2018.04.25
 */
public class SelectInfoService {
	public UserPojo SelectInfo(String nickname) throws UserException{
		UserPojoDao upd = new UserPojoDao();
		return upd.nicknameSelectOnInfo(nickname);
	}
}
