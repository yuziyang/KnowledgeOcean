package com.yzy97.test;

import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.dao.BookShelfPojoDao;
import com.yzy97.exception.BookException;

/**
 * BookShelfPojoDao类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.12
 */
public class BookShelfPojoDaoTest {
	private BookShelfPojoDao bspd;
	
	@Ignore
	@Test
	/**
	 * insert方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.12测试通过
	 */
	public void insertTest(){
		bspd = new BookShelfPojoDao();
		System.out.println(bspd.insert(18, 49, 0));
	}
	
	@Ignore
	@Test
	/**
	 * selectBId方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.13测试通过
	 */
	public void selectBIdTest(){
		bspd = new BookShelfPojoDao();
		System.out.println(bspd.selectBId(18, 49));
	}
	
	@Ignore
	@Test
	/**
	 * getDataNum方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.19测试通过
	 */
	public void getDataNumTest(){
		bspd = new BookShelfPojoDao();
		System.out.println(bspd.getDataNum(18, 0));
	}
	
	
	
	
	
}
