package com.yzy97.test;


import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;

/**
 * UserPojoDao类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.18
 */
public class UserPojoDaoTest {
	UserPojoDao userPD = new UserPojoDao();

	@Ignore
	@Test
	/**
	 * insert方法的测试方法
	 * @author yuziyang
	 * 2018.04.19  测试通过
	 */
	public void insertTest(){
		UserPojo userPojo = new UserPojo();
		userPojo.setUsername("yzy1997");
		userPojo.setPassword("yzy1997");
		userPojo.setuNickname("李四");
		
		System.out.println(userPD.insert(userPojo));
	}
		
	@Ignore	
	@Test
	/**
	 * nicknameToSelect方法的测试方法
	 * @author yuziyang
	 * 2018.04.18  测试通过
	 */
	public void nicknameToSelectTest(){
		String nickname = "张三";
		System.out.println(userPD.nicknameToSelect(nickname));
	}
	
	@Ignore
	@Test
	/**
	 * usernameToSelect方法的测试方法
	 * @author yuziyang
	 * 2018.04.18  测试通过
	 */
	public void usernameToSelectTest(){
		String username = "zs19970714";
		System.out.println(userPD.usernameToSelect(username));
	}
	
	@Ignore
	@Test
	/**
	 * usernameAndPasswordToSelect方法的测试方法
	 * @author yuziyang
	 * 2018.04.25  测试通过
	 */
	public void usernameAndPasswordToSelectTest(){
		try {
			System.out.println(userPD.usernameAndPasswordToSelect("yzy199707", "yzy19970714"));
		} catch (UserException e) {
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	/**
	 * usernameToSelectID方法的测试方法
	 * @author yuziyang
	 * 2018.04.25  测试通过
	 */
	public void usernameToSelectIDTest(){
		try {
			System.out.println(userPD.usernameToSelectID("yzy199707"));
		} catch (UserException e) {
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	/**
	 * usernameToSelectID方法的测试方法
	 * @author yuziyang
	 * 2018.04.25  测试通过
	 */
	public void createTableOnIdTest(){
		try {
			System.out.println(userPD.createTableOnId(userPD.usernameToSelectID("yzy199707")));
		} catch (UserException e) {
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	/**
	 * nicknameSelectOnInfo方法的测试方法
	 * @author yuziyang
	 * 2018.04.25  测试通过
	 */
	public void nicknameSelectOnInfoTest(){
		String str = "余子扬";
		try {
			UserPojo up = userPD.nicknameSelectOnInfo(str);
			System.out.println(up.getuSignature());
			System.out.println(up.getuPhone());
			System.out.println(up.getuEmail());
			System.out.println(up.getuPurview().toString());
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	/**
	 * nicknameUpdateOnInfo方法的测试方法
	 * @author yuziyang
	 * 2018.04.27  测试通过
	 */
	public void nicknameUpdateOnInfoTest(){
		UserPojo up = new UserPojo();
		up.setuNickname("张三");
		up.setuSignature("测试");
		up.setuPhone("");
		up.setuEmail("1404860325@qq.com");
		String oldNickname = "李四";
		System.out.println(userPD.nicknameUpdateOnInfo(oldNickname, up));
	}
	
	@Test
	/**
	 * nicknameToSelectID方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.12测试通过
	 */
	public void nicknameToSelectIDTest(){
		try {
			System.out.println(userPD.nicknameToSelectID("余子扬"));
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
