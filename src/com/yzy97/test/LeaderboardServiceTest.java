package com.yzy97.test;

import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.pojo.BookPojo;
import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.LeaderboardService;


/**
 * LeaderboardService类的测试方法
 * @version 1.0
 * @author yuziyang
 * @since 2018.05.17
 */
public class LeaderboardServiceTest {
	@Ignore
	@Test
	/**
	 * getPageInfo方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.17
	 */
	public void getPageInfoTest(){
		LeaderboardService ls = new LeaderboardService();
		PagingPojo pp = ls.getPageInfo(1, 3);
		System.out.println(pp.getCurrentPage());
		System.out.println(pp.getTotalPage());
		System.out.println(pp.getTotalData());
		
		for(BookPojo book : pp.getBooks()){
			System.out.println("--------------------------");
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
}
