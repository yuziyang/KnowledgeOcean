package com.yzy97.test;

import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.pojo.BookPojo;
import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.BookshelfService;
import com.yzy97.services.LeaderboardService;

/**
 * BookshelfService类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.19
 */
public class BookshelfServiceTest {
	@Ignore
	@Test
	/**
	 * getPageInfo方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.19测试通过
	 */
	public void getPageInfoTest(){
		BookshelfService bs = new BookshelfService();
		PagingPojo pp = bs.getPageInfo(1, 3, "余子扬", 0);
		System.out.println(pp.getCurrentPage());
		System.out.println(pp.getTotalPage());
		System.out.println(pp.getTotalData());
		
		for(BookPojo book : pp.getBooks()){
			System.out.println("--------------------------");
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
}
