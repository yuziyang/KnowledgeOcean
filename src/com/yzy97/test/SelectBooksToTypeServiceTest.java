package com.yzy97.test;

import org.junit.Test;

import com.yzy97.pojo.BookPojo;
import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.SelectBooksToTypeService;

/**
 * SelectBooksToTypeService类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.04
 */
public class SelectBooksToTypeServiceTest {
	@Test
	/**
	 * getData方法的测试方法
	 * @author yuziyang
	 * @since   2018.05.04测试通过
	 */
	public void getDataTest(){
		SelectBooksToTypeService sbtts = new SelectBooksToTypeService();
		PagingPojo pp = sbtts.getData(1, 12, "magic");
		System.out.println("总数据条数：" + pp.getTotalData());
		System.out.println("总页数：" + pp.getTotalPage());
		System.out.println("当前页：" + pp.getCurrentPage());
		System.out.println("-----------------------------");
		for(BookPojo book:pp.getBooks()){
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println("************");
		}
	}
}
