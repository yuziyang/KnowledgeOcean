package com.yzy97.test;

import java.io.BufferedReader;

import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.pojo.ReadingPojo;
import com.yzy97.util.FileUtil;

/**
 * FileUtil类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.06测试通过
 */
public class FileUtilTest {
	
	@Ignore
	@Test
	/**
	 * replaceToTimestamps方法的测试方法
	 * @author yuziyang
	 * @since 
	 */
	public void replaceToTimestampsTest(){
		FileUtil fu = FileUtil.getFileUtil();
		System.out.println(fu.replaceToTimestamps("余子扬.txt"));
	}
	
	
	@Test
	/**
	 * readFile方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.15测试通过
	 */
	public void readFileTest(){
		FileUtil fu = FileUtil.getFileUtil();
		ReadingPojo rp = fu.readFile("D:\\Tomcat\\webapps\\bookContext\\text\\xianxia\\1525607573773.txt","兔王","xianxia", "1525607573773.txt",1);
		System.out.println(rp.getCurrentPage());
		System.out.println(rp.getFinalyPage());
		System.out.println(rp.getType());
		System.out.println(rp.getFileName());
		System.out.println(rp.getContent());
	}
}
