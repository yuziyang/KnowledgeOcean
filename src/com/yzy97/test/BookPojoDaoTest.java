package com.yzy97.test;

import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;



import com.yzy97.dao.BookPojoDao;
import com.yzy97.exception.BookException;
import com.yzy97.pojo.BookPojo;

/**
 * BookPojoDao的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.30
 */
public class BookPojoDaoTest {
	/**书籍表操作的实体*/
	BookPojoDao bpd = null;
	
	@Ignore
	@Test
	/**
	 * typeToNum方法的测试方法
	 * @author yuziyang
	 * @since 2018.04.30测试通过
	 */
	public void typeToNumTest(){
		bpd = new BookPojoDao();
		try {
			System.out.print(bpd.typeToNum("cc"));
		} catch (BookException e) {
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	/**
	 * insert方法的测试方法
	 * @author yuziyang
	 * @since  2018.04.30测试通过
	 */
	public void insertTest(){
		BookPojo bp = new BookPojo();
		bpd = new BookPojoDao();
		bp.setbAuthor("余子扬");
		bp.setbName("我是谁");
		bp.setbDescription("测试测试测试测试");
		bp.setbType("xianxia");
		bp.setbCoveUrl("/KnowledgeOcean/imag/xianxia");
		bp.setbTextUrl("/KnowledgeOcean/book/xianxia");
		try {
			System.out.println(bpd.insert(bp));
		} catch (BookException e) {
			e.printStackTrace();
		}
	}
	
	@Ignore
	@Test
	/**
	 * dataNumToType方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.04 测试通过
	 */
	public void dataNumToTypeTest(){
		bpd = new BookPojoDao();
		System.out.println(bpd.dataNumToType("magic"));
	}
	
	@Ignore
	@Test
	/**
	 * booksToTypeAndPageNum方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.04测试通过
	 */
	public void booksToTypeAndPageNumTest(){
		bpd = new BookPojoDao();
		List<BookPojo> books = bpd.booksToTypeAndPageNum("magic", 0, 2);
		for(BookPojo book : books){
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println("--------------------------");
		}
	}
	
	@Ignore
	@Test
	/**
	 * selectBId方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.12测试通过
	 */
	public void selectBIdTest(){
		bpd = new BookPojoDao();
		System.out.println(bpd.selectBId("兔王", "yuziyang"));
	}
	
	@Ignore
	@Test
	/**
	 * collectionTimesPlus方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.13
	 */
	public void collectionTimesPlusTest(){
		bpd = new BookPojoDao();
		System.out.println(bpd.collectionTimesPlus(49));
	}
	
	@Ignore
	@Test
	/**
	 * getFilenaemAndType方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.14测试通过
	 */
	public void getFilenaemAndTypeTest(){
		bpd = new BookPojoDao();
		Map map = bpd.getFilenaemAndType("兔王", "yuziyang");
		System.out.println(map.get("fileName"));
		System.out.println(map.get("type"));
	}
	
	@Ignore
	@Test
	/**
	 * selectBooksTest方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.016测试通过
	 */
	public void selectBooksTest(){
		bpd = new BookPojoDao();
		List<BookPojo> books = bpd.selectBooks(0, 2);
		for(BookPojo book : books){
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
	
	@Ignore
	@Test
	/**
	 * getDataNum方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.17测试通过
	 */
	public void getDataNumTest(){
		bpd = new BookPojoDao();
		System.out.println(bpd.getDataNum());
	}
	
	@Ignore
	@Test
	/**
	 * getBooksToBookname方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.20测试通过
	 */
	public void getBooksToBooknameTest(){
		bpd = new BookPojoDao();
		List<BookPojo> books = bpd.getBooksToBookname("兔王", 0, 2);
		for(BookPojo book : books){
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
	
	@Ignore
	@Test
	/**
	 * getBooksToBookname方法的测试方法
	 * @author yuziyang
	 * @since 2018.05.20测试通过
	 */
	public void getBooksToAuthorTest(){
		bpd = new BookPojoDao();
		List<BookPojo> books = bpd.getBooksToAuthor("yuzi", 0, 2);
		for(BookPojo book : books){
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
	
	
	
	
}
