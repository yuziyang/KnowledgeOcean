package com.yzy97.test;

import org.junit.Test;

import com.yzy97.exception.UserException;
import com.yzy97.services.RegisterService;

/**
 *  RegisterService类的测试类
 *@version 1.0
 *@author yuziyang
 *@since 2018.04.19
 */
public class RegisterServiceTest {
	@Test
	/**
	 * register方法的测试方法
	 * 2018.04.19测试通过
	 */
	public void registerTest(){
		RegisterService rs = new RegisterService();
		String nickname = "余子扬";
		String username = "yzy19970714";
		String password = "yzy19970714";
		try {
			rs.register(nickname, username, password, null, null);
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
