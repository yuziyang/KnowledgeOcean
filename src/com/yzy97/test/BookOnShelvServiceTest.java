package com.yzy97.test;

import org.junit.Test;

import com.yzy97.exception.BookException;
import com.yzy97.services.BookOnShelvService;

/**
 * BookOnShelvService的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.30
 */
public class BookOnShelvServiceTest {
	@Test
	/**
	 * bookOnShelv方法的测试方法
	 * @author yuziyang
	 * @since 2018.04.30
	 */
	public void bookOnShelvTest(){
		BookOnShelvService boss = new BookOnShelvService();
		try {
			System.out.println(boss.BookOnShelv("你是谁", "cscscscsc", "余子扬", "magic", "/KnowledgeOcean/imag/magic", "/KnowledgeOcean/text/magic"));
		} catch (BookException e) {
			e.printStackTrace();
		}
	}
}
