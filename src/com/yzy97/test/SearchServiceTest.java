package com.yzy97.test;

import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.pojo.BookPojo;
import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.LeaderboardService;
import com.yzy97.services.SearchService;

/**
 * SearchService类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.20
 */
public class SearchServiceTest {
	@Ignore
	@Test
	/**
	 * getPageInfoToBookname方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.20测试通过
	 */
	public void getPageInfoToBooknameTest(){
		SearchService ss = new SearchService();
		PagingPojo pp = ss.getPageInfoToBookname("兔王", 1, 2);
		System.out.println(pp.getCurrentPage());
		System.out.println(pp.getTotalPage());
		System.out.println(pp.getTotalData());
		
		for(BookPojo book : pp.getBooks()){
			System.out.println("--------------------------");
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
	
	@Ignore
	@Test
	/**
	 * getPageInfoToAuthor方法的测试方法
	 * @author yuziyang
	 * @since  2018.05.20测试通过
	 */
	public void getPageInfoToAuthorTest(){
		SearchService ss = new SearchService();
		PagingPojo pp = ss.getPageInfoToAuthor("yu", 1, 2);
		System.out.println(pp.getCurrentPage());
		System.out.println(pp.getTotalPage());
		System.out.println(pp.getTotalData());
		
		for(BookPojo book : pp.getBooks()){
			System.out.println("--------------------------");
			System.out.println(book.getbId());
			System.out.println(book.getbName());
			System.out.println(book.getbDescription());
			System.out.println(book.getbAuthor());
			System.out.println(book.getbCoveUrl());
			System.out.println(book.getbTextUrl());
			System.out.println(book.getbType());
			System.out.println(book.getCollectionTimes());
			System.out.println("--------------------------");
		}
	}
	
	
	
	
	
	
	
	
}
