package com.yzy97.test;

import org.junit.Test;

import com.yzy97.util.StringUtil;

/**
 * StringUtil类的测试类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.19
 */
public class StringUtilTest {
	@Test
	/**
	 * convert方法的测试方法
	 * @author yuziyang
	 * @since  
	 */
	public void convertTest(){
		StringUtil su = StringUtil.getStringUtil();
		System.out.println(su.convert("xianxia"));
		System.out.println(su.convert("magic"));
		System.out.println(su.convert("city"));
		System.out.println(su.convert("history"));
		System.out.println(su.convert("science"));
		System.out.println(su.convert("literature"));
	}
}
