
package com.yzy97.test;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;



import org.junit.Ignore;
import org.junit.Test;

import com.yzy97.util.DBUtil;
/**
 * DBUtil类的测试类
 * @version  1.0
 *@author yuziyang
 *@since 2018.04.17
 */
public class DBUtilTest {
	@Test
	/**
	 * getConn方法的测试方法
	 * @author yuziyang
	 * @since 2018.04.17测试通过
	 */
	public void getConnTest(){
		DBUtil dbUtil = DBUtil.getDBUtil();
		Connection conn = dbUtil.getConn();
		try {
			if(conn.isValid(0)){
				System.out.println("连接数据库成功！");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
}
