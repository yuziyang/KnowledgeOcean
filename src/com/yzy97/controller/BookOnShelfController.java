package com.yzy97.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import com.yzy97.exception.BookException;
import com.yzy97.services.BookOnShelvService;
import com.yzy97.util.FileUtil;
/**
 * 上架书籍的逻辑
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.14
 */
@WebServlet(name = "BookOnShelfController" , urlPatterns = {"/bookOnShelf"})
public class BookOnShelfController extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		/**封面图片名称*/
		String coveImagName = null;
		/**书名*/
		String name = null;
		/**简介*/
		String description = null;
		/**作者*/
		String author = null;
		/**类型*/
		String type = null;
		/**文本名称*/
		String textName = null;
	
		FileUtil fu = FileUtil.getFileUtil();
		
		//使用fileupload包完成文件上传
		/**
		 * 1.创建工厂
		 * 2.使用工厂创建解析器对象
		 * 3.使用解析器对象解析req，得到FileItem列表
		 * 4.具体操作
		 */
		//创建工厂
		DiskFileItemFactory dfif = new DiskFileItemFactory();
		//使用工厂创建解析器对象
		ServletFileUpload sfu = new ServletFileUpload(dfif);
		/**封面图片的FileItem对象*/
		FileItem coveImagFileItem = null;
		/**文本的FileItem对象*/
		FileItem textFileItem = null;
		sfu.setHeaderEncoding("utf-8");
		try {
			//使用解析器对象解析req，得到FileItem列表
			List<FileItem> list = sfu.parseRequest(req);
			
			for(FileItem item:list){
				if(item.isFormField()){
					//表单项为普通项
					String filedName = item.getFieldName();
					switch(filedName){
					case "img_name":
						coveImagName = fu.replaceToTimestamps(item.getString("utf-8"));
						break;
					case "bookname":
						name = item.getString("utf-8");
						break;
					case "description":
						description = item.getString("utf-8");
						break;
					case "authorname":
						author = item.getString("utf-8");
						break;
					case "type":
						type = item.getString("utf-8");
						break;
					case "book_name":
						textName = fu.replaceToTimestamps(item.getString("utf-8"));
						break;
					}
				}else{
					//表单项为文件项
					String filedName = item.getFieldName();
					switch(filedName){
					case "add_img":
						coveImagFileItem = item;
						break;
					case "add_book":
						textFileItem = item;
						break;
					}
				}
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
		}
		
		//上传文件到服务器
		String contentPath = this.getServletContext().getRealPath(File.separator);
		int endIndex = contentPath.indexOf(File.separator + "KnowledgeOcean" + File.separator);
		String pathname = contentPath.substring(0,endIndex); 
		/**封面路径*/
		String coveImgUrl = pathname + File.separator + "bookContext" + File.separator + "coveImg" + File.separator + type + File.separator + coveImagName;
		/**文本路径*/
		String textUrl = pathname + File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + textName;
		 
		try {
			fu.uploadFile(coveImagFileItem, coveImgUrl);
			fu.uploadFile(textFileItem, textUrl);
		} catch (BookException e) {
			req.setAttribute("information", e.getMessage());
		}
		
		//录入书籍信息到数据库
		BookOnShelvService boss = new BookOnShelvService();
		try {
			boss.BookOnShelv(name, description, author, type, coveImagName, textName);
		} catch (BookException e) {
			req.setAttribute("information", e.getMessage());
		}
		req.getRequestDispatcher("/jsp/right/book_on_shelv.jsp").forward(req, resp);
	}
	
}
