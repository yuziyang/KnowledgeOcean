package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.SelectBooksToTypeService;
/**
 * 分类查看的控制器
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.05
 */
@WebServlet(name = "CategoryBooklistController",urlPatterns = {"/categoryBooklist"})
public class CategoryBooklistController extends HttpServlet {
	/**每页显示的数据个数*/
	private final int dataPerPage = 6;
	
	
	/**用于处理初始界面*/
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setAttribute("currentPage", req.getParameter("currentPage"));
		req.setAttribute("type", req.getParameter("type"));
		req.getSession().setAttribute("type", req.getParameter("type"));
		doPost(req, resp);
	}

	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		/**当前页*/
		int currentPage = Integer.parseInt((String)req.getAttribute("currentPage"));
		/**类型*/
		String type = (String)req.getAttribute("type");
		SelectBooksToTypeService sbtts = new SelectBooksToTypeService();
		PagingPojo pp = sbtts.getData(currentPage, dataPerPage, type);
		//当前页码
		req.getSession().setAttribute("currentPage", pp.getCurrentPage().toString());
		//总数据数
		req.getSession().setAttribute("totalData", pp.getTotalData().toString());
		//总页数
		req.getSession().setAttribute("totalPage", pp.getTotalPage().toString());
		//书籍总信息
		req.getSession().setAttribute("books", pp.getBooks());
	}
	
}
