package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;
import com.yzy97.services.UpdateInfoService;
/**
 * 更新个人信息的逻辑
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.14
 */
@WebServlet(name = "UpdateInfoController",urlPatterns = {"/updateInfo"})
public class UpdateInfoController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		/*原昵称*/
		String oldNickname = (String)req.getSession().getAttribute("nickname");
		/*新昵称*/
		String nickname = req.getParameter("nickname");
		/*新个性签名*/
		String signature = req.getParameter("signature");
		/*新手机号码*/
		String phone = req.getParameter("phone");
		/*新邮箱号*/
		String email = req.getParameter("email");
		/*插入标志*/
		Exception exception = null;
		/*初始化错误消息*/
		req.setAttribute("information", "");
		
		UpdateInfoService uis = new UpdateInfoService();
		UserPojo up = new UserPojo();
		up.setuNickname(nickname);
		up.setuSignature(signature);
		up.setuPhone(phone);
		up.setuEmail(email);
		try {
			uis.update(oldNickname, up);
		} catch (UserException e) {
			req.setAttribute("information", e.getMessage());
			exception = e;
		}
		
		if(exception == null){
			HttpSession httpSession = req.getSession();
			httpSession.setAttribute("nickname", nickname);
			httpSession.setAttribute("signature", signature);
			httpSession.setAttribute("phone",phone );
			httpSession.setAttribute("email",email );
		}
 	}
	
}
