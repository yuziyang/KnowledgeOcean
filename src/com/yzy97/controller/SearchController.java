package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.util.Introspection;

import com.yzy97.pojo.BookPojo;
import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.SearchService;

/**
 * 搜索的逻辑
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.20
 */
@WebServlet(name = "SearchController" , urlPatterns = {"/search"})
public class SearchController extends HttpServlet {
	/**每页显示的数据数*/
	private static final int dataPerPage = 6;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String searchType = req.getParameter("searchType");
		req.getSession().setAttribute("searchType", searchType);
		String searchString = req.getParameter("searchString");
		req.getSession().setAttribute("searchString", searchString);
		int currentPage = Integer.parseInt((String)req.getParameter("currentPage"));
		PagingPojo pp = new PagingPojo();
		SearchService ss = new SearchService();
		if(searchType.equals("author")){
			pp = ss.getPageInfoToAuthor(searchString, currentPage, SearchController.dataPerPage);
		}else if(searchType.equals("bookname")){
			pp = ss.getPageInfoToBookname(searchString, currentPage, SearchController.dataPerPage);
		}
		req.getSession().setAttribute("searchPageInfo", pp);
	}
	
}
