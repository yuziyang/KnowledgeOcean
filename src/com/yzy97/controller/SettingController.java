package com.yzy97.controller;

import java.io.IOException;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.yzy97.dao.UserPojoDao;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;
import com.yzy97.services.SelectInfoService;
/**
 * 处理个人页面请求
 *@version 1.0
 *@author yuziyang
 *@since 2018.04.25
 */
@WebServlet(name = "SettingController" , urlPatterns = {"/setting"})
public class SettingController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		UserPojo up = null;
		SelectInfoService sis = new SelectInfoService();
		HttpSession session = req.getSession();
		String nickname = (String)session.getAttribute("nickname");
		try {
			up = sis.SelectInfo(nickname);
			session.setAttribute("signature", up.getuSignature());
			session.setAttribute("phone", up.getuPhone());
			session.setAttribute("email", up.getuEmail());
			session.setAttribute("purview", up.getuPurview().toString());
			req.getRequestDispatcher("/jsp/left/setting.jsp").forward(req, resp);
		} catch (UserException e) {
			req.setAttribute("information", e.getMessage());
		}
		
	}
	
}
