package com.yzy97.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yzy97.pojo.ReadingPojo;
import com.yzy97.services.ReadService;

/**
 * 阅读器的逻辑
 * 
 * @version 1.0
 * @author yuziyang
 * @since 2018.05.14
 */
@WebServlet(name = "ReadingController", urlPatterns = { "/reading" })
public class ReadingController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int currentPage = Integer.parseInt(req.getParameter("currentPage"));
		String bookname = req.getParameter("bookname"); 
		String type = req.getParameter("type");
		String fileName = req.getParameter("fileName"); 
		// 获取tomcat的webapp目录路径
		String contentPath = this.getServletContext().getRealPath(
				File.separator);
		int endIndex = contentPath.indexOf(File.separator + "KnowledgeOcean"
				+ File.separator);
		String pathname = contentPath.substring(0, endIndex);
		
		ReadService rs = new ReadService();
		ReadingPojo rp = rs.onlineRead(pathname, bookname, type, fileName, currentPage);
		req.getSession().setAttribute("readingPojo", rp);
	}

}
