package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.LeaderboardService;
/**
 * 排行榜查看的逻辑
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.17
 */
@WebServlet(name = "LeaderboardController",urlPatterns = {"/leaderboardBooklist"})
public class LeaderboardController extends HttpServlet {
	/**每页显示的数据个数*/
	private final static int dataPerPage = 10;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int currentPage = Integer.parseInt((String)req.getParameter("currentPage"));
		LeaderboardService ls = new LeaderboardService();
		PagingPojo pp = new PagingPojo();
		pp = ls.getPageInfo(currentPage, LeaderboardController.dataPerPage);
		req.getSession().setAttribute("leaderboardPagingPojo", pp);
	}
	
}
