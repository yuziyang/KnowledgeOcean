package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yzy97.exception.UserException;
import com.yzy97.services.RegisterService;

/**
 * 处理注册请求
 * @version 1.0
 * @author yuziyang
 * @since 2018.04.21
 */
@WebServlet(name = "RegisterController" , urlPatterns = {"/register"})
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		/*用户昵称*/
		String nickname = request.getParameter("nickname");
		/*用户名*/
		String username = request.getParameter("username");
		/*密码*/
		String password = request.getParameter("password");
		/*手机号*/
		String phone = request.getParameter("phone") == null?"":request.getParameter("phone");
		/*邮箱*/
		String email = request.getParameter("email") == null?"":request.getParameter("email");
		//初始化错误信息
		request.setAttribute("information", "");
		
		Exception exception = null;
		
		
		RegisterService rs = new RegisterService();
		try {
			rs.register(nickname, username, password, phone, email);
		} catch (UserException e) {
			request.setAttribute("information", e.getMessage());
			exception = e;
		}
		
		if(exception == null){
			request.getRequestDispatcher("/jsp/new/registerInformation.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("/jsp/new/register.jsp").forward(request, response);
		}
	}

}
