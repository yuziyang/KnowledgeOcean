package com.yzy97.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yzy97.exception.BookException;
import com.yzy97.exception.UserException;
import com.yzy97.pojo.ReadingPojo;
import com.yzy97.services.AddToBookshelfService;
import com.yzy97.services.ReadService;
/**
 * 加入收藏的逻辑
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.14
 */
@WebServlet(name = "AddToBookshelfController",urlPatterns = {"/addToBookshelf"})
public class AddToBookshelfController extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		int bsType = Integer.parseInt(req.getParameter("bsType"));
		if(bsType == 0){
			//加入收藏
			addToCollection(req, resp);
		}else{
			//在线阅读
			onlineRead(req, resp);
		}
	}
	
	/**
	 * 加入收藏
	 * @author yuziyang
	 * @param req
	 * @param resp
	 * @since 2018.05.13
	 */
	public void addToCollection(HttpServletRequest req,HttpServletResponse resp){
		String nickname = req.getParameter("nickname");
		String bookname = req.getParameter("bookname");
		String author = req.getParameter("author");
		int bsType = Integer.parseInt(req.getParameter("bsType"));
		AddToBookshelfService atbs = new AddToBookshelfService();
		try {
			atbs.add(nickname, bookname, author, bsType);
		} catch (Exception e) {
			req.setAttribute("information", e.getMessage());
		}
	}
	
	/**
	 * 在线阅读
	 * @author yuziyang
	 * @param req
	 * @param resp
	 * @throws IOException 
	 * @throws ServletException 
	 * @since 2018.05.14
	 */
	public void onlineRead(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{
		String nickname = req.getParameter("nickname");
		String bookname = req.getParameter("bookname");
		String author = req.getParameter("author");
		int currentPage = Integer.parseInt(req.getParameter("currentPage"));
		int bsType = Integer.parseInt(req.getParameter("bsType"));
		//获取tomcat的webapp目录路径
		String contentPath = this.getServletContext().getRealPath(File.separator);
		int endIndex = contentPath.indexOf(File.separator + "KnowledgeOcean" + File.separator);
		String pathname = contentPath.substring(0,endIndex); 
		AddToBookshelfService atbs = new AddToBookshelfService();
		ReadService rs = new ReadService();
		ReadingPojo rp = new ReadingPojo();
		try {
			atbs.add(nickname, bookname, author, bsType);
			rp = rs.onlineRead(pathname, bookname, author, currentPage);
			req.getSession().setAttribute("readingPojo", rp);
			req.getRequestDispatcher("/jsp/new/reading.jsp").forward(req, resp);
		} catch (UserException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
