package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yzy97.pojo.PagingPojo;
import com.yzy97.services.BookshelfService;

/**
 * 书架的逻辑
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.19
 */
@WebServlet(name = "BookshelfControll" , urlPatterns = {"/bookshelf"})
public class BookshelfControll extends HttpServlet {
	/**每页显示数据*/
	private static int dataPerPage = 6;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		int bsType = Integer.parseInt((String)req.getParameter("bsType"));
		int currentPage = Integer.parseInt((String)req.getParameter("currentPage"));
		String nickname = (String)req.getParameter("nickname");
		BookshelfService bs = new BookshelfService();
		PagingPojo pp = bs.getPageInfo(currentPage, BookshelfControll.dataPerPage, nickname, bsType);
		req.getSession().setAttribute("bookshelfPagInfo", pp);
	}
	
}
