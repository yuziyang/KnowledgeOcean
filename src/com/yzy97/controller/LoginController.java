package com.yzy97.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.yzy97.exception.UserException;
import com.yzy97.services.LoginService;
/**
 * 处理登录请求
 * @version 1.0 
 *@author yuziyang
 *@since 2018.04.23
 */
@WebServlet(name="LoginController" , urlPatterns={"/login"})
public class LoginController extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		//昵称
		String nickname = null;
		//用户名
		String username = req.getParameter("username");
		//密码
		String password = req.getParameter("password");
		//初始化错误信息
	    req.setAttribute("information", "");
	    
	    LoginService ls = new LoginService();
	    Exception exception = null;
		try {
			nickname = ls.login(username, password);
		} catch (UserException e) {
			req.setAttribute("information", e.getMessage());
			exception = e;
		}
		
		if(exception == null){
			HttpSession session = req.getSession();
			session.setAttribute("nickname",nickname );
			req.getRequestDispatcher("/jsp/new/loginInformation.jsp").forward(req, resp);
		}else{
			req.getRequestDispatcher("/jsp/new/login.jsp").forward(req, resp);
		}
	}
	
}
