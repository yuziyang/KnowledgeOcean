package com.yzy97.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.apache.commons.fileupload.FileItem;

import com.yzy97.exception.BookException;
import com.yzy97.pojo.ReadingPojo;

/**
 * 文件操作工具类
 * 
 * @version 1.0
 * @author yuziyang
 * @since 2018.05.02
 */
public class FileUtil {
	private static FileUtil fu = null;

	private FileUtil() {
	}

	/**
	 * 取得FileUtil的唯一实例
	 * 
	 * @author yuziyang
	 * @return FileUtil的唯一实例
	 * @since 2018.05.02
	 */
	public static FileUtil getFileUtil() {
		if (fu == null) {
			fu = new FileUtil();
		}
		return fu;
	}

	/**
	 * 写入文件到服务器
	 * 
	 * @author yuziyang
	 * @param fi
	 *            代表文件的commons-fileupload的FileItem实例
	 * @param url
	 *            需要上传到的位置（带文件名）
	 * @return true:成功 false：失败
	 * @throws BookException
	 * @since 2018.05.02
	 */
	public void uploadFile(FileItem fi, String url) throws BookException {
		// 文件
		File file = new File(url);
		// 目录
		String dir = url.substring(0, url.lastIndexOf(File.separator));
		File dirFile = new File(dir);
		try {
			// 目录不存在则创建
			if (!dirFile.isDirectory()) {
				dirFile.mkdirs();
			}

			if (!file.exists()) {
				file.createNewFile();
			}
			fi.write(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BookException("文件写入服务器失败！");
		}
	}

	/**
	 * 替换原文件名为 时间戳+后缀
	 * 
	 * @author yuziyang
	 * @param oldFilename
	 *            原文件名
	 * @return 新文件名
	 * @since 2018.05.06
	 */
	public String replaceToTimestamps(String oldFilename) {
		int index = oldFilename.lastIndexOf(".");
		String suffix = oldFilename.substring(index);
		String timestamps = new Long(System.currentTimeMillis()).toString();
		String newFilename = timestamps + suffix;
		return newFilename;
	}

	/**
	 * 读取文件
	 * 
	 * @author yuziyang
	 * @param url
	 *            文件路径
	 * @param bookname
	 *            书名
	 * @param type
	 *            文件类别
	 * @param fileName
	 *            文件名
	 * @param page
	 *            需要读取的页码
	 * @return 阅读器实体
	 * @since 2018.05.13
	 */
	public ReadingPojo readFile(String url, String bookname, String type,
			String fileName, int currentPage) {
		ReadingPojo rp = new ReadingPojo();
		boolean finalyPage = false;
		char[] ch = new char[2048];
		File file = new File(url);
		if (!file.exists()) {
			System.out.println("文件不存在！");
			return null;
		}

		try {
			
			FileReader fr = new FileReader(file);
			fr.skip((currentPage - 1) * 2048);
			for (int i = 0; i < 2048; i++) {
				if (fr.read(ch, i, 1) == -1) {
					finalyPage = true;
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String content = "";
		for (int i = 0; i < 2048; i++) {
			if (ch[i] == '\0') {
				break;
			} else if (ch[i] == ' ') {
				content += "&nbsp";
			}else if(ch[i] == '\r'){
				content += "<br/>";
			}else{
				content += ch[i];
			}
		}
		rp.setCurrentPage(currentPage);
		rp.setFinalyPage(finalyPage);
		rp.setContent(content);
		rp.setBookname(bookname);
		rp.setType(type);
		rp.setFileName(fileName);
		return rp;
	}
}
