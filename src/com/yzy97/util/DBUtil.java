package com.yzy97.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 数据库工具类
 * @version 1.0
 *@author yuziyang
 *@since  2018.04.17
 */
public class DBUtil {
	/*数据库工具类实例*/
	private static DBUtil dbu = null;
	private DBUtil(){}
	
	/**
	 * 获得DBUtil的实例
	 * @author yuziyang
	 * @return DBUtil的实例
	 * @since 2018.04.18
	 */
	public static DBUtil getDBUtil(){
		if(dbu == null){
			dbu = new DBUtil();
		}
		return dbu;
	}
	
	
	/**
	 * 获得数据库连接
	 * @author yuziyang
	 * @return   连接
	 * @since   2018.04.17
	 */
	public Connection getConn(){
		Connection conn = null;
		try {
			String driverClassName = null;
			String dbUrl = null;
			String user = null;
			String password = null;
			String pathname = this.getClass().getResource("/").getPath();
			File file = new File(pathname + "DB.property");
			
			if(file.isFile() && file.exists()){
				Properties properties = new Properties();
				properties.load(new FileReader(file));
				driverClassName = properties.getProperty("driverClassName");
				dbUrl = properties.getProperty("dbUrl");
				user = properties.getProperty("user");
				password = properties.getProperty("password");
			}else{
				try{
					throw new Exception("配置文件不存在！");
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
			Class.forName(driverClassName);
			conn = DriverManager.getConnection(dbUrl,user,password);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			
		}
		return conn;
	}
	
	/**
	 * 检查Resultset结果集是否存在数据
	 * @author yuziyang
	 * @param rs 需要检查的结果集
	 * @return true:存在数据  false:不存在数据
	 * @since 2018.04.18
	 */
	public boolean resultsetExistData(ResultSet rs){
		int num = 0;
		try {
			while(rs.next()){
				num++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			rs.beforeFirst();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(num > 0){
			return true;
		}else{
			return false;
		}
		
	} 
	
	/**
	 * 关闭数据库连接资源
	 * @author yuziyang
	 * @param conn  需要关闭的数据库连接实例
	 * @param ps    需要关闭的预编译sql语句实例
	 * @since  2018.04.18
	 */
	public void close(Connection conn,PreparedStatement ps){
		try {
			if(conn != null){
				conn.close();
			}
			if(ps != null){
				ps.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 关闭数据库连接资源
	 * @author yuziyang
	 * @param conn  需要关闭的数据库连接实例
	 * @param ps    需要关闭的预编译sql语句实例
	 * @param rs    需要关闭的结果集
	 * @since  2018.04.18
	 */
	public void close(Connection conn,PreparedStatement ps,ResultSet rs){
		try {
			if(conn != null){
				conn.close();
			}
			if(ps != null){
				ps.close();
			}
			if(rs != null){
				rs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
