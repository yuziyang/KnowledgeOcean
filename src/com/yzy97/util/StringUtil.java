package com.yzy97.util;

import jdk.internal.dynalink.beans.StaticClass;

/**
 * 字符串工具类
 * 
 * @version 1.0
 * @author yuziyang
 * @since 2018.05.19
 */
public class StringUtil {
	private static StringUtil su = null;

	private StringUtil() {
	};

	/**
	 * 取得StringUtil的唯一实例
	 * 
	 * @author yuziyang
	 * @return StringUtil的唯一实例
	 * @since 2018.05.19
	 */
	public static StringUtil getStringUtil() {
		if (su == null) {
			su = new StringUtil();
		}
		return su;
	}

	/**
	 * 转换书籍类型为中文
	 * 
	 * @author yuziyang
	 * @param type
	 *            书籍类型（英）
	 * @return 书籍类型
	 * @since 2018.05.19
	 */
	public String convert(String type) {
		String string = null;
		switch (type) {
		case "xianxia":
			string = "仙侠";
			break;
		case "magic":
			string = "魔幻";
			break;
		case "city":
			string = "都市";
			break;
		case "history":
			string = "历史";
			break;
		case "science":
			string = "科幻";
			break;
		case "literature":
			string = "文学";
			break;
		}
		return string;
	}

}
