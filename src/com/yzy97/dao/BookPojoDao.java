package com.yzy97.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yzy97.exception.BookException;
import com.yzy97.pojo.BookPojo;
import com.yzy97.util.DBUtil;
import com.yzy97.util.StringUtil;

/**
 * 操作书籍表的类
 * @version 1.0
 *@author yuziyang
 *@since 2018.04.30
 */
public class BookPojoDao {
	/**数据库工具类实例 */
	private DBUtil dbu = null;
	/**数据库连接实例*/
	private Connection conn = null;
	/**预编译的sql语句实例*/
	private PreparedStatement ps = null;
	/** 结果集*/
	private ResultSet rs = null;
	
	/**
	 * 根据类型名返回类型编号
	 * @author yuziyang
	 * @param type  需要查询的类型名
	 * @return    类型的对应编号,为0表示出现错误
	 * @throws BookException 
	 * @since 2018.04.30
	 */
	public int typeToNum(String type) throws BookException{
		int num = 0;
		String sql = "select bType_id from btype where b_type = ?";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, type);
			rs = ps.executeQuery();
			if(!dbu.resultsetExistData(rs)){
				throw new BookException("书籍类型不存在");
			}
			rs.next();
			num = rs.getInt(1);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps,rs);
		}
		return num;
	}
	
	/**
	 * 向数据库插入书籍
	 * @author yuziyang
	 * @param bp  需要插入的书籍
	 * @return  true:成功      false：失败
	 * @throws BookException 
	 * @since 2018.04.30
	 */
	public boolean insert(BookPojo bp) throws BookException{
		int num = 0;
		String sql = "insert into book(b_name,b_description,"
				+ "b_author,bType_id,b_cover_url,b_text_url) "
				+ "values(?,?,?,?,?,?)";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			int typeNum = typeToNum(bp.getbType());
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, bp.getbName());
			ps.setString(2, bp.getbDescription());
			ps.setString(3, bp.getbAuthor());
			ps.setInt(4, typeNum);
			ps.setString(5, bp.getbCoveUrl());
			ps.setString(6, bp.getbTextUrl());
			num = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps);
		}
		
		if(num > 0){
			return true;
		}else{
			return false;
		}
		
	}
	
	/**
	 * 根据类型返回数据条数
	 * @author yuziyang
	 * @param type  需要查询的类型
	 * @return   数据条数
	 * @since 2018.05.04
	 */
	public int dataNumToType(String type){
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		int num = 0;
		String sql = "select count(b_name) from book,btype "
				+ "where book.bType_id = btype.bType_id && btype.b_type = ?;"; 
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, type);
			rs = ps.executeQuery();
			rs.next();
			num = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return num;
	}
	
	/**
	 * 根据页码和类型查询图书
	 * @author yuziyang
	 * @param type  类型
	 * @param beginIndex  需要查询的起始位置（包含）
	 * @param dataPerPage   查询的书籍数
	 * @return 查询到的所有图书数据
	 * @since 2018.05.04
	 */
	public List<BookPojo> booksToTypeAndPageNum(String type, int beginIndex,int dataPerPage){
		List<BookPojo> books = new ArrayList<BookPojo>();
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		String sql = "select b_id,b_name,b_description,b_author,b_cover_url,b_text_url "
				+ "from book,btype "
				+ "where book.bType_id = btype.bType_id && btype.b_type = ? "
				+ "limit ?,?;";
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, type);
			ps.setInt(2, beginIndex);
			ps.setInt(3, dataPerPage);
			rs = ps.executeQuery();
			while(rs.next()){
				BookPojo bp = new BookPojo();
				bp.setbId(rs.getInt(1));
				bp.setbName(rs.getString(2));
				bp.setbDescription(rs.getString(3));
				bp.setbAuthor(rs.getString(4));
				String bCoveUrl = rs.getString(5);
				//需要对获得的文件名进行拼接
				bp.setbCoveUrl(File.separator + "bookContext" + File.separator + "coveImg" + File.separator + type + File.separator + bCoveUrl);
				String bTextUrl = rs.getString(6);
				bp.setbTextUrl(File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + bTextUrl);
				books.add(bp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		
		return books;
	}
	
	/**
	 * 根据书名和作者查询编号
	 * @author yuziyang
	 * @param bookname
	 * @param author
	 * @return 书籍编号
	 * @since 2018.05.12
	 */
	public int selectBId(String bookname,String author){
		String sql = "select b_id from book where b_name=? && b_author=?";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		int bId = 0;
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, bookname);
			ps.setString(2, author);
			rs = ps.executeQuery();
			rs.next();
			bId = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return bId;	
	}
	
	/**
	 * 根据书籍id增加书籍收藏数
	 * @author yuziyang
	 * @param bId  书籍id
	 * @return  true:成功 false:失败
	 */
	public boolean collectionTimesPlus(int bId){
		int num = 0;
		String sql = "update book set collection_times=collection_times+1 where b_id=?";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, bId);
			num = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps);
		}
		
		if(num > 0){
			return true;
		}else{
			return false;
		}
		
	}
	
	/**
	 * 根据书名和作者获取书籍文件名和类型
	 * @param bookname   书名
	 * @param author     作者
	 * @return    fileName和type的map集合
	 */
	public Map getFilenaemAndType(String bookname,String author){
		String sql = "select b_text_url,btype.b_type from book,btype where b_name=? && b_author=? && book.bType_id=btype.bType_id";
		String fileName = null;
		String type = null;
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, bookname);
			ps.setString(2, author);
			rs = ps.executeQuery();
			rs.next();
			fileName = rs.getString(1);
			type = rs.getString(2);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		
		Map maps = new HashMap(); 
		maps.put("fileName", fileName);
		maps.put("type", type);
		return maps;
	}
	
	/**
	 * 根据收藏数查询书籍
	 * @author yuziyang
	 * @param beginIndex  需要查询的起始位置（包含）
	 * @param dataPerPage   查询书籍数
	 * @return 查询到的所有图书数据
	 * @since 2018.05.16
	 */
	public List<BookPojo> selectBooks(int beginIndex,int dataPerPage){
		List<BookPojo> books = new ArrayList<BookPojo>();
		String sql = "select b_id,b_name,b_description,b_author,b_cover_url,b_text_url,btype.b_type,collection_times "
				+ "from book,btype "
				+ "where book.bType_id = btype.bType_id "
				+ "order by collection_times desc limit ?,?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, beginIndex);
			ps.setInt(2, dataPerPage);
			rs = ps.executeQuery();
			while(rs.next()){
				BookPojo bp = new BookPojo();
				bp.setbId(rs.getInt(1));
				bp.setbName(rs.getString(2));
				bp.setbDescription(rs.getString(3));
				bp.setbAuthor(rs.getString(4));
				StringUtil su = StringUtil.getStringUtil(); 
				String type = rs.getString(7);
				bp.setbType(su.convert(type));
				String bCoveUrl = rs.getString(5);
				//需要对获得的文件名进行拼接
				bp.setbCoveUrl(File.separator + "bookContext" + File.separator + "coveImg" + File.separator + type + File.separator + bCoveUrl);
				String bTextUrl = rs.getString(6);
				bp.setbTextUrl(File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + bTextUrl);
				bp.setCollectionTimes(rs.getInt(8));
				books.add(bp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		
		return books;
	}
	
	/**
	 * 获取book表的总数据条数
	 * @author yuziyang
	 * @return 总数据条数
	 * @since 2018.05.17
	 */
	public int getDataNum(){
		int total = 0;
		String sql = "select count(b_id) from book;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.next();
			total = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return total;
	}
	
	
	/**
	 * 查询相应用户书籍表中指定状态的书籍
	 * @author yuziyang
	 * @param uId 用户id
	 * @param bsType  书籍的状态，   0：收藏       1：正在读       2：已读完
	 * @param beginIndex  需要查询的起始位置（包含）
	 * @param dataPerPage   查询书籍数
	 * @return 查询到的所有图书数据
	 * @since 2018.05.19
	 */
	public List<BookPojo> getBooks(int uId,int bsType,int beginIndex,int dataPerPage){
		List<BookPojo> books = new ArrayList<BookPojo>();
		String bookshelfTable = uId + "_bookshelf ";
		String sql = "select book.b_id,b_name,b_description,b_author,b_cover_url,b_text_url,btype.b_type,collection_times "
				+ "from book,btype," + bookshelfTable  
				+ "where book.bType_id = btype.bType_id "
				+ "&&" + bookshelfTable + ".bs_type = ? " + "&&" + bookshelfTable +".b_id = book.b_id limit ?,?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, bsType);
			ps.setInt(2, beginIndex);
			ps.setInt(3, dataPerPage);
			rs = ps.executeQuery();
			
			while(rs.next()){
				BookPojo bp = new BookPojo();
				bp.setbId(rs.getInt(1));
				bp.setbName(rs.getString(2));
				bp.setbDescription(rs.getString(3));
				bp.setbAuthor(rs.getString(4));
				StringUtil su = StringUtil.getStringUtil(); 
				String type = rs.getString(7);
				bp.setbType(su.convert(type));
				String bCoveUrl = rs.getString(5);
				//需要对获得的文件名进行拼接
				bp.setbCoveUrl(File.separator + "bookContext" + File.separator + "coveImg" + File.separator + type + File.separator + bCoveUrl);
				String bTextUrl = rs.getString(6);
				bp.setbTextUrl(File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + bTextUrl);
				bp.setCollectionTimes(rs.getInt(8));
				books.add(bp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return books;
	}
	
	
	/**
	 * 根据书名关键字获取book表的总数据条数
	 * @author yuziyang
	 * @param  书名关键字
	 * @return 总数据条数
	 * @since 2018.05.17
	 */
	public int getDataNumToBookname(String bookname){
		int total = 0;
		String sql = "select count(b_id) from book where b_name like ?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + bookname + "%");
			rs = ps.executeQuery();
			rs.next();
			total = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return total;
	}
	
	
	/**
	 * 根据作者名关键字获取book表的总数据条数
	 * @author yuziyang
	 * @param   author  书名关键字
	 * @return 总数据条数
	 * @since 2018.05.17
	 */
	public int getDataNumToAuthor(String author){
		int total = 0;
		String sql = "select count(b_id) from book where b_author like ?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + author + "%");
			rs = ps.executeQuery();
			rs.next();
			total = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return total;
	}
	
	/**
	 * 根据书名关键字查询所有书籍
	 * @author yuziyang
	 * @param bookname   书名关键字
	 * @param beginIndex  需要查询的起始位置（包含）
	 * @param dataPerPage   查询的书籍数
	 * @return 查询到的所有图书数据
	 * @since 2018.05.19
	 */
	public List<BookPojo> getBooksToBookname(String bookname,int beginIndex,int dataPerPage){
		List<BookPojo> books = new ArrayList<BookPojo>();
		String sql = "select book.b_id,b_name,b_description,b_author,b_cover_url,b_text_url,btype.b_type,collection_times "
				+ "from book,btype "
				+ "where book.bType_id = btype.bType_id && b_name like ?"
				+ " limit ?,?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + bookname + "%");;
			ps.setInt(2, beginIndex);
			ps.setInt(3, dataPerPage);
			rs = ps.executeQuery();
			
			while(rs.next()){
				BookPojo bp = new BookPojo();
				bp.setbId(rs.getInt(1));
				bp.setbName(rs.getString(2));
				bp.setbDescription(rs.getString(3));
				bp.setbAuthor(rs.getString(4));
				StringUtil su = StringUtil.getStringUtil(); 
				String type = rs.getString(7);
				bp.setbType(su.convert(type));
				String bCoveUrl = rs.getString(5);
				//需要对获得的文件名进行拼接
				bp.setbCoveUrl(File.separator + "bookContext" + File.separator + "coveImg" + File.separator + type + File.separator + bCoveUrl);
				String bTextUrl = rs.getString(6);
				bp.setbTextUrl(File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + bTextUrl);
				bp.setCollectionTimes(rs.getInt(8));
				books.add(bp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return books;
	}
	
	/**
	 * 根据作者名关键字查询所有书籍
	 * @author yuziyang
	 * @param author   作者名关键字
	 * @param beginIndex  需要查询的起始位置（包含）
	 * @param lastIndex   需要查询的终止位置（不包含）
	 * @return 查询到的所有图书数据
	 * @since 2018.05.19
	 */
	public List<BookPojo> getBooksToAuthor(String author,int beginIndex,int lastIndex){
		List<BookPojo> books = new ArrayList<BookPojo>();
		String sql = "select book.b_id,b_name,b_description,b_author,b_cover_url,b_text_url,btype.b_type,collection_times "
				+ "from book,btype "
				+ "where book.bType_id = btype.bType_id && b_author like ?"
				+ " limit ?,?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + author + "%");;
			ps.setInt(2, beginIndex);
			ps.setInt(3, lastIndex);
			rs = ps.executeQuery();
			
			while(rs.next()){
				BookPojo bp = new BookPojo();
				bp.setbId(rs.getInt(1));
				bp.setbName(rs.getString(2));
				bp.setbDescription(rs.getString(3));
				bp.setbAuthor(rs.getString(4));
				StringUtil su = StringUtil.getStringUtil(); 
				String type = rs.getString(7);
				bp.setbType(su.convert(type));
				String bCoveUrl = rs.getString(5);
				//需要对获得的文件名进行拼接
				bp.setbCoveUrl(File.separator + "bookContext" + File.separator + "coveImg" + File.separator + type + File.separator + bCoveUrl);
				String bTextUrl = rs.getString(6);
				bp.setbTextUrl(File.separator + "bookContext" + File.separator + "text" + File.separator + type + File.separator + bTextUrl);
				bp.setCollectionTimes(rs.getInt(8));
				books.add(bp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		return books;
	}
	
	
	
	
}
