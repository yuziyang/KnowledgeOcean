package com.yzy97.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.yzy97.exception.BookException;
import com.yzy97.util.DBUtil;

/**
 * 操作书架表的类
 * @version 1.0
 *@author yuziyang
 *@since 2018.05.12
 */
public class BookShelfPojoDao {
	
	/**数据库工具类实例 */
	private DBUtil dbu = null;
	/**数据库连接实例*/
	private Connection conn = null;
	/**预编译的sql语句实例*/
	private PreparedStatement ps = null;
	/** 结果集*/
	private ResultSet rs = null;
	
	/**
	 * 插入书架数据
	 * @param uId   当前用户的id，用于查找对应书架表
	 * @param bId   需要添加到书架的书籍id
	 * @param bsType  书籍的状态，   0：收藏       1：正在读       2：已读完
	 * @return true:成功 false:失败
	 * @since 2018.05.12
	 */
	public boolean insert(int uId,int bId,int bsType){
		int num = 0;
		String tableName = uId + "_bookshelf";
		String sql = "insert into " + tableName + " values(?,?);";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, bId);
			ps.setInt(2, bsType);
			num = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps);
		}
		if(num > 0){
			return true;
		}else{
			return false;
		}	
	}
	
	/**
	 * 更新书籍在书架的状态
	 * @param uId   当前用户的id，用于查找对应书架表
	 * @param bId   需要添加到书架的书籍id
	 * @param bsType  书籍的状态，   0：收藏       1：正在读       2：已读完
	 * @return true:成功 false:失败
	 * @since 2018.05.13
	 */
	public boolean updateBsType(int uId,int bId,int bsType){
		int num = 0;
		String tableName = uId + "_bookshelf";
		String sql = "update " + tableName + " set bs_type=? where b_id=?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, bsType);
			ps.setInt(2, bId);
			num = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps);
		}
		if(num > 0){
			return true;
		}else{
			return false;
		}	
	}
	
	/**
	 * 根据用户id和书籍id查询记录
	 * @param uID  用户id
	 * @param bId  书籍id
	 * @return  true:存在   false:不存在
	 * @throws BookException 
	 * @since 2018.05.13
	 */
	public boolean selectBId(int uId,int bId){
		boolean bool = false;
		String tableName = uId + "_bookshelf";
		String sql = "select b_id from " + tableName + " where b_id = ?"; 
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, bId);
			rs = ps.executeQuery();
			bool = dbu.resultsetExistData(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		
		return bool;
	}
	
	/**
	 * 获得相应状态的书籍数目
	 * @author yuziyang
	 * @param uId 用户id
	 * @param bsType 书籍的状态，   0：收藏       1：正在读       2：已读完
	 * @return 书籍数
	 * @since 2018.05.19
	 */
	public int getDataNum(int uId,int bsType){
		int num = 0;
		String tableName = uId + "_bookshelf"; 
		String sql = " select count(b_id) from " + tableName +" where bs_type = ?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setInt(1, bsType);
			rs = ps.executeQuery();
			rs.next();
			num = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps, rs);
		}
		
		return num;
	}
	
}
