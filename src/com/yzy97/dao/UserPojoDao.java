package com.yzy97.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.yzy97.exception.UserException;
import com.yzy97.pojo.UserPojo;
import com.yzy97.util.DBUtil;

/**
 * 操作用户表的类
 * 
 * @version 1.0
 * @author yuziyang
 * @since 2018.04.18
 */
public class UserPojoDao {
	/**数据库工具类实例 */
	private DBUtil dbu = null;
	/**数据库连接实例*/
	private Connection conn = null;
	/**预编译的sql语句实例*/
	private PreparedStatement ps = null;

	/**
	 *   向数据库插入用户
	 * @author yuziyang
	 * @param user  用户
	 * @return 成功返回true，失败返回false
	 * @since 2018.04.18
	 */
	public boolean insert(UserPojo user){
		/*用户昵称 */
		String nickname = user.getuNickname();
		/*用户名 */
		String username = user.getUsername();
		 /*密码 */
		String password = user.getPassword();
		 /*手机号 */
		String phone = user.getuPhone() == null?"":user.getuPhone();
		/* 邮箱 */
		String email = user.getuEmail() == null?"":user.getuEmail();
		/*sql语句执行后影响的行数*/
		int num = 0;
		dbu = DBUtil.getDBUtil();
		String sql = "insert into user(u_nickname,u_name,u_password,u_phone,u_email) values(?,?,?,?,?)";
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, nickname);
			ps.setString(2, username);
			ps.setString(3, password);
			ps.setString(4, phone);
			ps.setString(5, email);
			num = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps);
		}
		
		if(num > 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 根据昵称查询是否存在记录
	 * 
	 * @author yuziyang
	 * @param nickname
	 *            需要查询的昵称
	 * @return 存在返回true，不存在返回false
	 * @since 2018.04.18
	 */
	public boolean nicknameToSelect(String nickname) {
		boolean bool = false;
		dbu = DBUtil.getDBUtil();
		ResultSet rs = null;
		String sql = "select u_nickname from user where u_nickname = ?";
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, nickname);
			rs = ps.executeQuery();
			bool = dbu.resultsetExistData(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbu.close(conn, ps,rs);
		}

		return bool;
	}

	/**
	 * 根据用户名查询是否存在记录
	 * 
	 * @author yuziyang
	 * @param nickname
	 *            需要查询的昵称
	 * @return 存在返回true，不存在返回false
	 * @since 2018.04.18
	 */
	public boolean usernameToSelect(String username) {
		boolean bool = false;
		dbu = DBUtil.getDBUtil();
		ResultSet rs = null;
		String sql = "select u_name from user where u_name = ?";
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			bool = dbu.resultsetExistData(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbu.close(conn, ps,rs);
		}

		return bool;
	}
	
	/**
	 * 根据用户名和密码查询
	 * @author yuziyang
	 * @param username    用户名
	 * @param password    密码
	 * @return   返回查询到的昵称 
	 * @throws UserException
	 * @since 2018.04.23
	 */
	public String usernameAndPasswordToSelect(String username,String password) throws UserException{
		boolean bool = false;
		String nickname = null;
		dbu = DBUtil.getDBUtil();
		ResultSet rs = null;
		String sql = "select u_nickname from user where u_name = ? and u_password = ?";
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			rs = ps.executeQuery();
			bool = dbu.resultsetExistData(rs);
			
			if(bool == false){
				throw new UserException("用户名或者密码错误");
			}else{
				rs.beforeFirst();
				rs.next();
				nickname = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbu.close(conn, ps,rs);
		}
		
		return nickname;
	}
	
	
	/**
	 * 根据用户名查询用户id
	 * @author yuziyang
	 * @param username    用户名
	 * @return   查询到的用户id 
	 * @throws UserException
	 * @since 2018.04.23
	 */
	public int usernameToSelectID(String username) throws UserException{
		boolean bool = false;
		int id = 0;
		dbu = DBUtil.getDBUtil();
		ResultSet rs = null;
		String sql = "select u_id from user where u_name = ?";
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			rs = ps.executeQuery();
			bool = dbu.resultsetExistData(rs);
			
			if(bool == false){
				throw new UserException("用户名或者密码错误");
			}else{
				rs.beforeFirst();
				rs.next();
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbu.close(conn, ps,rs);
		}
		
		return id;
	}
	
	
	/**
	 * 根据用户id创建书架表
	 * @author yuziyang
	 * @param id   用户id
	 * @return   true：成功创建   false：创建失败
	 * @throws UserException 
	 * @since  2018.04.23
	 */
	public boolean createTableOnId(int id) throws UserException{
		boolean bool = false;
		dbu = DBUtil.getDBUtil();
		String tableName = id + "_bookshelf";
		String sql = "create table " + tableName +"("
				+ "b_id int not null primary key,"
				+ "bs_type int not null,"
				+ "foreign key(b_id) references book(b_id));";
		 
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new UserException("创建书架失败！");
		} finally {
			dbu.close(conn, ps);
		}
		
		return true;
	}
	
	/**
	 * 根据昵称查询个人信息
	 * @author yuziyang
	 * @param nickname   用户昵称
	 * @return  查询到的个人信息
	 * @throws UserException 
	 * @since  2018.04.25
	 */
	public UserPojo nicknameSelectOnInfo(String nickname) throws UserException{
		UserPojo userPojo = new UserPojo();
		dbu = DBUtil.getDBUtil();
		ResultSet rs = null;
		
		String sql = "select u_signature,u_phone,u_email,u_purview from user where u_nickname = ?";
		
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, nickname);
			rs = ps.executeQuery();
			while(rs.next()){
				userPojo.setuSignature(rs.getString(1));
				userPojo.setuPhone(rs.getString(2));
				userPojo.setuEmail(rs.getString(3));
				userPojo.setuPurview(rs.getInt(4));
			}
		} catch (SQLException e) {
		    e.printStackTrace();
			throw new UserException("查询个人信息错误！");
		} finally {
			dbu.close(conn, ps,rs);
		}
		return userPojo;
	}
	
	/**
	 * 根据昵称更新个人信息
	 * @author yuziyang
	 * @param up   需要更新的个人信息
	 * @return     true：成功  false：失败
	 * @since 2018.04.27
	 */
	public boolean nicknameUpdateOnInfo(String oldNickname,UserPojo up){
		/*需要更新的昵称*/
		String nickname = up.getuNickname();
		/*需要更新的个性签名*/
		String signature = up.getuSignature();
		/*需要更新的手机号*/
		String phone = up.getuPhone();
		/*需要更新的邮箱*/
		String email = up.getuEmail();
		/*sql语句执行后影响的行数*/
		int num = 0;
		
		String sql = "update user set u_nickname=?,u_signature=?,"
				+ "u_phone=?,u_email=?" 
                 + "where u_nickname=?;";
		dbu = DBUtil.getDBUtil();
		conn = dbu.getConn();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, nickname);
			ps.setString(2, signature);
			ps.setString(3, phone);
			ps.setString(4, email);
			ps.setString(5, oldNickname);
			num = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			dbu.close(conn, ps);
		}
		if(num > 0){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 根据昵称查询用户id
	 * @author yuziyang
	 * @param nickname    昵称
	 * @return   查询到的用户id 
	 * @throws UserException
	 * @since 2018.04.23
	 */
	public int nicknameToSelectID(String nickname) throws UserException{
		boolean bool = false;
		int id = 0;
		dbu = DBUtil.getDBUtil();
		ResultSet rs = null;
		String sql = "select u_id from user where u_nickname = ?";
		try {
			conn = dbu.getConn();
			ps = conn.prepareStatement(sql);
			ps.setString(1, nickname);
			rs = ps.executeQuery();
			bool = dbu.resultsetExistData(rs);
			
			if(bool == false){
				throw new UserException("昵称有误！");
			}else{
				rs.beforeFirst();
				rs.next();
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbu.close(conn, ps,rs);
		}
		
		return id;
	}
	
}
