 //预览图片+显示图片名称
	function previewFile() {
		var preview = document.getElementById("picture");
		var file = document.getElementById("add_img");
		var file_name = document.getElementById("img_name");
		var reader = new FileReader();
		reader.onloadend = function() {
			preview.src = reader.result;
			file_name.value = file.value.substring(file.value.lastIndexOf("\\") + 1);
		}
		if (file) {
			reader.readAsDataURL(file.files[0]);
		} else {
			preview.src = "";
		}
	}
    //显示选中文件名称
    function displayName(){
    	var file = document.getElementById("add_book");
    	var file_name = document.getElementById("book_name");
    	file_name.value = file.value.substring(file.value.lastIndexOf("\\") + 1);
    }
    //提交表单
    function submitForm(){
    	var form = document.getElementById("form");
    	form.submit();
    }