    /*
     * 昵称正确性标志
     * true：正确     false：错误
     */
    var nicknameFlag = false;
    /*
     * 用户名正确性标志
     * true：正确     false：错误
     */
    var usernameFlag = false;
    /*
     * 密码正确性标志
     * true：正确     false：错误
     */
    var passwordFlag = false;
    /*
     * 确认密码正确性标志
     * true：正确     false：错误
     */
    var passwordTFlag = false;
    /*
     * 手机号正确性标志
     * true：正确     false：错误
     */
    var phoneFlag = true;
    /*
     * 邮箱正确性标志
     * true：正确     false：错误
     */
    var emailFlag = true;
   /**
     * 当组件获得焦点时初始化
     * @param id 组件id
     */
    function init(id){
    	switch(id){
    	case "nickname":
    		if(document.getElementById("nickname_comment").innerHTML != "正确"){
    		    document.getElementById("nickname_comment").innerHTML = "请输入2-12个字符的中英文(昵称为你社区显示名称)";
    		    document.getElementById("nickname").value = "";
    		}
    		break;
    	case "username":
    		if(document.getElementById("username_comment").innerHTML != "正确"){
    		    document.getElementById("username_comment").innerHTML = "请输入5-16个字符的英文或数字(用户名为你登录账户)";
    		    document.getElementById("username").value = "";
    		}
    		break;
    	case "password":
    		if(document.getElementById("password_comment").innerHTML != "正确"){
    		    document.getElementById("password_comment").innerHTML = "请输入5-16个字符的英文或数字";
    		    document.getElementById("password").value = "";
    		}
    		break;
    	case "password_t":
    		if(document.getElementById("password_comment").innerHTML != "正确"){
    		    document.getElementById("password_t_comment").innerHTML = "请正确输入密码之后再确认密码";
    		    document.getElementById("password_t").value = "";
    		    document.getElementById("password_t").blur();
    		    return;
    		}
    		break;
    	case "phone":
    		if(document.getElementById("phone_comment").innerHTML != "正确"){
    		    document.getElementById("phone_comment").innerHTML = "请输入您的11位手机号(可为空)";
    		    document.getElementById("phone").value = "";
    		}
    		break;
    	case "email":
    		if(document.getElementById("email_comment").innerHTML != "正确"){
    		    document.getElementById("email_comment").innerHTML = "请输入您的联系邮箱(可为空)";
    		    document.getElementById("email").value = "";
    		}
    		break;
    	}
    }
    
    /**
     * 昵称合格性检验
     */
    function nicknameExamine(){
    	var str = document.getElementById("nickname").value;
    	//非空检验
    	if(str == ""){
    		document.getElementById("nickname_comment").innerHTML = "昵称不能为空！";
    		nicknameFlag = false;
    		return;
    	}
    	//长度检验
    	if(str.length < 2){
    		document.getElementById("nickname_comment").innerHTML = "昵称太短！";
    		nicknameFlag = false;
    		return;
    	}
    	//正则表达式检验
    	var reg = /[a-zA-Z\u4e00-\u9fa5]*/;
    	var temp = str.match(reg);
    	if(temp.toString().length != str.length){
    		document.getElementById("nickname_comment").innerHTML = "昵称含有非法字符！";
    		nicknameFlag = false;
    		return;
    	}
    	
    	document.getElementById("nickname_comment").innerHTML = "正确";
    	nicknameFlag = true;
    }
    
    /**
     * 用户名合格性检验
     */
    function usernameExamine(){
    	var str = document.getElementById("username").value;
    	//非空检验
    	if(str == ""){
    		document.getElementById("username_comment").innerHTML = "用户名不能为空！";
    		usernameFlag = false;
    		return;
    	}
    	//长度检验
    	if(str.length <= 5){
    		document.getElementById("username_comment").innerHTML = "用户名太短！";
    		usernameFlag = false;
    		return;
    	}
    	//正则表达式检验
    	var reg = /[a-zA-Z0-9]*/;
    	var temp = str.match(reg);
    	if(temp.toString().length != str.length){
    		document.getElementById("username_comment").innerHTML = "用户名含有非法字符！";
    		usernameFlag = false;
    		return;
    	}
    	
    	document.getElementById("username_comment").innerHTML = "正确";
    	usernameFlag = true;
    }
    
    /**
     * 密码合格性检验
     */
    function passwordExamine(){
    	var str = document.getElementById("password").value;
    	//非空检验
    	if(str == ""){
    		document.getElementById("password_comment").innerHTML = "密码不能为空！";
    		passwordFlag = false;
    		return;
    	}
    	//长度检验
    	if(str.length <= 5){
    		document.getElementById("password_comment").innerHTML = "密码太短！";
    		passwordFlag = false;
    		return;
    	}
    	//正则表达式检验
    	var reg = /[a-zA-Z0-9]*/;
    	var temp = str.match(reg);
    	if(temp.toString().length != str.length){
    		document.getElementById("password_comment").innerHTML = "密码含有非法字符！";
    		passwordFlag = false;
    		return;
    	}
    	
    	document.getElementById("password_comment").innerHTML = "正确";
    	passwordFlag = true;
    }
    
    /**
     * 确认密码合格性检验
     */
    function passwordTExamine(){
    	var str1 = document.getElementById("password").value;
    	var str2 = document.getElementById("password_t").value;
    	
    	if(str2 == ""){
    		document.getElementById("password_t_comment").innerHTML = "请输入密码之后再输入";
    		passwordTFlag = false;
    		return;
    	}
    	
    	if(str2 != str1){
    		document.getElementById("password_t_comment").innerHTML = "确认密码不正确";
    		passwordTFlag = false;
    		return;
    	}
    	
    	document.getElementById("password_t_comment").innerHTML = "正确";
    	passwordTFlag = true;
    }
    
    /*
     * 手机号合格性检验
     */
    function phoneExamine(){
    	var str = document.getElementById("phone").value;
    	var reg = /[0-9]{11}/;
    	
    	if(str == ""){
    		phoneFlag = true;
    		return;
    	}
    	
    	if(!reg.test(str)){
    		document.getElementById("phone_comment").innerHTML = "手机号不正确";
    		phoneFlag = false;
    		return;
    	}
    	
    	document.getElementById("phone_comment").innerHTML = "正确";
    	phoneFlag = true;
    }
    
    /*
     * 邮箱合格性检验
     */
    function emailExamine(){
    	var str = document.getElementById("email").value;
    	var reg = /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/;
    	
    	if(str == ""){
    		emailFlag = true;
    		return;
    	}
    	
    	if(!reg.test(str)){
    		document.getElementById("email_comment").innerHTML = "邮箱不正确";
    		emailFlag = false;
    		return;
    	}
    	
    	document.getElementById("email_comment").innerHTML = "正确";
    	emailFlag = true;
    }
    
    
    /*
     * 注册按钮
     */
    function register(){
    	var form = document.getElementById("form");
    	if(nicknameFlag && usernameFlag && passwordFlag && passwordTFlag && phoneFlag && emailFlag){
    		form.submit();
    	}else{
    		alert("输入信息有误，请仔细检查！");
    	}
    }
    
    