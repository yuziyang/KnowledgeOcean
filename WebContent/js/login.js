    /*
     * 用户名正确性标志
     * true：正确     false：错误
     */
    var usernameFlag = false;
    /*
     * 密码正确性标志
     * true：正确     false：错误
     */
    var passwordFlag = false;


    /*
     * 当组件获得焦点时初始化
     * @param id 组件id
     */
    function init(id){
    	switch(id){
    	case "username":
    		if(document.getElementById("username_comment").innerHTML != "正确"){
    		    document.getElementById("username_comment").innerHTML = "请输入5-16个字符的英文或数字(用户名为你登录账户)";
    		}
    		break;
    	case "password":
    		if(document.getElementById("password_comment").innerHTML != "正确"){
    		    document.getElementById("password_comment").innerHTML = "请输入5-16个字符的英文或数字";
    		}
    		break;
    	}
    }
    
    /**
     * 用户名合格性检验
     */
    function usernameExamine(){
    	var str = document.getElementById("username").value;
    	//非空检验
    	if(str == ""){
    		document.getElementById("username_comment").innerHTML = "用户名不能为空！";
    		usernameFlag = false;
    		return;
    	}
    	//长度检验
    	if(str.length <= 5){
    		document.getElementById("username_comment").innerHTML = "用户名太短！";
    		usernameFlag = false;
    		return;
    	}
    	//正则表达式检验
    	var reg = /[a-zA-Z0-9]*/;
    	var temp = str.match(reg);
    	if(temp.toString().length != str.length){
    		document.getElementById("username_comment").innerHTML = "用户名含有非法字符！";
    		usernameFlag = false;
    		return;
    	}
    	
    	document.getElementById("username_comment").innerHTML = "正确";
    	usernameFlag = true;
    }
    
    /**
     * 密码合格性检验
     */
    function passwordExamine(){
    	var str = document.getElementById("password").value;
    	//非空检验
    	if(str == ""){
    		document.getElementById("password_comment").innerHTML = "密码不能为空！";
    		passwordFlag = false;
    		return;
    	}
    	//长度检验
    	if(str.length <= 5){
    		document.getElementById("password_comment").innerHTML = "密码太短！";
    		passwordFlag = false;
    		return;
    	}
    	//正则表达式检验
    	var reg = /[a-zA-Z0-9]*/;
    	var temp = str.match(reg);
    	if(temp.toString().length != str.length){
    		document.getElementById("password_comment").innerHTML = "密码含有非法字符！";
    		passwordFlag = false;
    		return;
    	}
    	
    	document.getElementById("password_comment").innerHTML = "正确";
    	passwordFlag = true;
    }
    
    /*
     * 登录
     */
    function register(){
    	var form = document.getElementById("form");
    	if(usernameFlag && passwordFlag){
    		form.submit();
    	}else{
    		alert("输入信息有误，请仔细检查！");
    	}
    }
    