//上一页
function pevPage(bookname,fileName,type,currentPage){
	if(currentPage == "1"){
		alert("已经是第一页了！");
		return;
	}else{
		xhrRequest(bookname,fileName,type,--currentPage);
	}
}
//下一页
function nextPage(bookname,fileName,type,currentPage,finalyPage){
	if(finalyPage == "true"){
		alert("已经是最后一页了");
		return;
	}else{
		xhrRequest(bookname,fileName,type,++currentPage);
	}
}
//翻页的ajax
function xhrRequest(bookname,fileName,type,currentPage){
	request = "/KnowledgeOcean/reading" + "?bookname=" + bookname + "&fileName=" + fileName + "&type=" + type + "&currentPage=" + currentPage;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", request);
	xhr.send(null);
	xhr.onreadystatechange = function(){
		window.location.reload(true);
	}
}