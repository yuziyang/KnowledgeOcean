var div;
//分类查看
function forwordCategory(request, id) {
	// 高亮显示选中菜单
	if (div != null) {
		div.style.backgroundColor = "#99D3AD";
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
		request += "?type=xianxia&currentPage=1";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request);
		xhr.send(null);
		xhr.onreadystatechange = function(){
    		window.open("/KnowledgeOcean/jsp/right/category_booklist.jsp","right");
    	}
	} else if (div == null) {
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
		request += "?type=xianxia&currentPage=1";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request);
		xhr.send(null);
		xhr.onreadystatechange = function(){
    		window.open("/KnowledgeOcean/jsp/right/category_booklist.jsp","right");
    	}	
	}

}

//排行榜查看
function forwordLeaderboard(request,id) {
	// 高亮显示选中菜单
	if (div != null) {
		div.style.backgroundColor = "#99D3AD";
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
		request += "?currentPage=1";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request);
		xhr.send(null);
		xhr.onreadystatechange = function(){
    		window.open("/KnowledgeOcean/jsp/right/leaderboard_booklist.jsp","right");
    	}
	} else if (div == null) {
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
		request += "?currentPage=1";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request);
		xhr.send(null);
		xhr.onreadystatechange = function(){
    		window.open("/KnowledgeOcean/jsp/right/leaderboard_booklist.jsp","right");
    	}	
	}

}

//书架查看
function forwordBookshelf(request,id,nickname) {
	// 高亮显示选中菜单
	if (div != null) {
		div.style.backgroundColor = "#99D3AD";
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
		request += "?currentPage=1&nickname=" + nickname + "&bsType=1";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request);
		xhr.send(null);
		xhr.onreadystatechange = function(){
    		window.open("/KnowledgeOcean/jsp/right/bookshelf_reading.jsp","right");
    	}
	} else if (div == null) {
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
		request += "?currentPage=1&nickname=" + nickname + "&bsType=1";
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request);
		xhr.send(null);
		xhr.onreadystatechange = function(){
    		window.open("/KnowledgeOcean/jsp/right/bookshelf_reading.jsp","right");
    	}	
	}

}

//搜索
function search() {
	var select = document.getElementById("search_type");
	var index = select.selectedIndex;
	var value = select.options[index].value;
	var string = document.getElementById("search_string").value;
	var request = "/KnowledgeOcean/search"
	request += "?searchType=" + value + "&currentPage=1&searchString=" + string;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", request);
	xhr.send(null);
	xhr.onreadystatechange = function(){
		window.open("/KnowledgeOcean/jsp/new/search_result.jsp","_parent");
	}
}


// 向后台请求
function request(nickname, url, id) {
	// 高亮显示选中菜单
	if (div != null) {
		div.style.backgroundColor = "#99D3AD";
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
	} else if (div == null) {
		div = document.getElementById(id);
		div.style.backgroundColor = "blue";
	}
	// 控制跳转到个人页面还是登录页面
	var nicknameStr = nickname;
	if (nicknameStr != "") {
		top.right.location.href = url;
		document.getElementById("nickname").href = "/KnowledgeOcean//jsp/left/setting.jsp";
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "/KnowledgeOcean/setting");
		xhr.send(null);
	} else {
		window.open("/KnowledgeOcean/jsp/new/login.jsp", "_parent");
	}
}

// 跳转
function login(url) {
	window.open(url, "_parent");
}