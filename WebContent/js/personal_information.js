    //数据校验（忽略个性签名）
     /*
     * 昵称正确性标志
     * true：正确     false：错误
     */
    var nicknameFlag = true;
    /*
     * 手机号正确性标志
     * true：正确     false：错误
     */
    var phoneFlag = true;
    /*
     * 邮箱正确性标志
     * true：正确     false：错误
     */
    var emailFlag = true;
    
    /**
     * 当组件获得焦点时初始化
     * @param id 组件id
     */
    function init(id,original){
    	switch(id){
    	case "text_nickname":
    		if(document.getElementById("nickname_comment").innerHTML != "正确"){
    		    document.getElementById("nickname_comment").innerHTML = "请输入2-12个字符的中英文(昵称为你社区显示名称)";
    		    document.getElementById("text_nickname").value = original;
    		}
    		break;
    	case "text_phone":
    		if(document.getElementById("phone_comment").innerHTML != "正确"){
    		    document.getElementById("phone_comment").innerHTML = "请输入您的11位手机号(可为空)";
    		    document.getElementById("text_phone").value = original;
    		}
    		break;
    	case "text_email":
    		if(document.getElementById("email_comment").innerHTML != "正确"){
    		    document.getElementById("email_comment").innerHTML = "请输入您的联系邮箱(可为空)";
    		    document.getElementById("text_email").value = original;
    		}
    		break;
    	}
    }
    
    /**
     * 昵称合格性检验
     */
    function nicknameExamine(){
    	var str = document.getElementById("text_nickname").value;
    	//非空检验
    	if(str == ""){
    		document.getElementById("nickname_comment").innerHTML = "昵称不能为空！";
    		nicknameFlag = false;
    		return;
    	}
    	//长度检验
    	if(str.length < 2){
    		document.getElementById("nickname_comment").innerHTML = "昵称太短！";
    		nicknameFlag = false;
    		return;
    	}
    	//正则表达式检验
    	var reg = /[a-zA-Z\u4e00-\u9fa5]*/;
    	var temp = str.match(reg);
    	if(temp.toString().length != str.length){
    		document.getElementById("nickname_comment").innerHTML = "昵称含有非法字符！";
    		nicknameFlag = false;
    		return;
    	}
    	
    	document.getElementById("nickname_comment").innerHTML = "正确";
    	nicknameFlag = true;
    }
    
    /*
     * 手机号合格性检验
     */
    function phoneExamine(){
    	var str = document.getElementById("text_phone").value;
    	var reg = /[0-9]{11}/;
    	
    	if(str == ""){
    		phoneFlag = true;
    		document.getElementById("phone_comment").innerHTML = "正确";
    		return;
    	}
    	
    	if(!reg.test(str)){
    		document.getElementById("phone_comment").innerHTML = "手机号不正确";
    		phoneFlag = false;
    		return;
    	}
    	
    	document.getElementById("phone_comment").innerHTML = "正确";
    	phoneFlag = true;
    }
    
    /*
     * 邮箱合格性检验
     */
    function emailExamine(){
    	var str = document.getElementById("text_email").value;
    	var reg = /[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?/;
    	
    	if(str == ""){
    		document.getElementById("email_comment").innerHTML = "正确";
    		emailFlag = true;
    		return;
    	}
    	
    	if(!reg.test(str)){
    		document.getElementById("email_comment").innerHTML = "邮箱不正确";
    		emailFlag = false;
    		return;
    	}
    	
    	document.getElementById("email_comment").innerHTML = "正确";
    	emailFlag = true;
    }
    
    //编辑转换保存
    function edit(){
    	//使输入框能获得焦点
    	document.getElementById("text_nickname").disabled = false;
    	document.getElementById("text_signature").disabled = false;
    	document.getElementById("text_phone").disabled = false;
    	document.getElementById("text_email").disabled = false;
    	//使输入框可写
    	document.getElementById("text_nickname").readOnly = false;
    	document.getElementById("text_signature").readOnly = false;
    	document.getElementById("text_phone").readOnly = false;
    	document.getElementById("text_email").readOnly = false;
    	
    	document.getElementById("nickname_comment").innerHTML = "请输入2-12个字符的中英文(昵称为你社区显示名称)";
    	document.getElementById("signature_comment").innerHTML = "请输入您的个性签名(可为空)";
    	document.getElementById("phone_comment").innerHTML = "请输入您的11位手机号(可为空)";
    	document.getElementById("email_comment").innerHTML = "请输入您的联系邮箱(可为空)";
    	document.getElementById("operator").value = "保存";
    }
    //保存转换编辑
    function save(){
    	//使输入框不能获得焦点
    	document.getElementById("text_nickname").disabled = true;
    	document.getElementById("text_signature").disabled = true;
    	document.getElementById("text_phone").disabled = true;
    	document.getElementById("text_email").disabled = true;
    	//使输入框只读
    	document.getElementById("text_nickname").readOnly = true;
    	document.getElementById("text_signature").readOnly = true;
    	document.getElementById("text_phone").readOnly = true;
    	document.getElementById("text_email").readOnly = true;
    	
    	document.getElementById("nickname_comment").innerHTML = "";
    	document.getElementById("signature_comment").innerHTML = "";
    	document.getElementById("phone_comment").innerHTML = "";
    	document.getElementById("email_comment").innerHTML = "";
    	
    	document.getElementById("operator").value = "编辑";
    }
    //根据value来执行
    function operator(){
    	var str = document.getElementById("operator").value;
    	if(str == "编辑"){
    		edit();
    	}else if(str == "保存"){
    		submit();
    		save();
    	}
    }
    

    //提交数据到后台
    function submit(){
    	var nickname = document.getElementById("text_nickname").value;
    	var signature = document.getElementById("text_signature").value;
    	var phone = document.getElementById("text_phone").value;
    	var email = document.getElementById("text_email").value;
    	
    	if(nicknameFlag && phoneFlag && emailFlag){
    		var xhr = new XMLHttpRequest();
        	xhr.open("post", "/KnowledgeOcean/updateInfo");
        	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        	xhr.send("nickname=" + nickname +
        			"&signature=" + signature +
        			"&phone=" + phone +
        			"&email=" + email);
        	xhr.onreadystatechange = function(){
        		window.location.reload(true);
        		window.open("/KnowledgeOcean/jsp/top.jsp","top");
        	}
    	}else{
    		operator();
    	}
    	
    }
