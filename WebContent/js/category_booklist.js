/*检验数据合理性*/
function operator(id,totalPage,type){
	var currentPage = document.getElementById("currentPage").getAttribute("value");
	switch(id){
	    case "previous":
		    if(currentPage == 1){
		    	alert("这已经是第一页了！");
		    }else{
		    	--currentPage;
		    	xhrRequest(currentPage,type);
		    }
		    break;
	    case "next":
	    	if(currentPage == totalPage){
	    		alert("这已经是最后一页了！");
	    	}else{
	    		++currentPage;
	    		xhrRequest(currentPage,type);
	    	}
	    	break;
	    case "go":
	    	var pageNum = document.getElementById("pageNum").value;
	    	if(pageNum < 1 || pageNum > totalPage){
	    		alert("请输入正确的页数！");
	    	}else{
	    		currentPage = pageNum;
	    		xhrRequest(currentPage,type);
	    	}
	    	break;
	}
}
//翻页的ajax
function xhrRequest(currentPage,type){
	request = "/KnowledgeOcean/categoryBooklist" + "?type=" + type+ "&currentPage=" + currentPage;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", request);
	xhr.send(null);
	xhr.onreadystatechange = function(){
		window.location.reload(true);
		window.open("/KnowledgeOcean/jsp/right/category_booklist.jsp","right");
	}
}

/**
 * 加入收藏的ajax
 * @author yuziyang
 * @param username  用户名
 * @param bookname  书名
 * @param author    作者
 * @param bsType    在书架中的状态  0：收藏 1：正在读
 */ 
function add(nickname,bookname,author,bsType){
	if(nickname == ""){
		alert("请先登录再操作！");
		return;
	}
	request = "/KnowledgeOcean/addToBookshelf?nickname=" + nickname + "&bookname=" + bookname + "&author=" + author + "&bsType=" + bsType;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", request);
	xhr.send(null);
}

/**
 * 在线阅读的ajax
 * @author yuziyang
 * @param username  用户名
 * @param bookname  书名
 * @param author    作者
 * @param bsType    在书架中的状态  0：收藏 1：正在读
 * @param currentPage 当前页码
 */ 
function read(nickname,bookname,author,bsType){
	if(nickname == ""){
		alert("请先登录再操作！");
		return;
	}
	request = "/KnowledgeOcean/addToBookshelf?nickname=" + nickname + "&bookname=" + bookname + "&author=" + author + "&bsType=" + bsType + "&currentPage=1";
	var xhr = new XMLHttpRequest();
	xhr.open("GET", request);
	xhr.send(null);
	xhr.onreadystatechange = function(){
		window.open("/KnowledgeOcean/jsp/new/reading.jsp","_top");
	}
}