/*
Navicat MySQL Data Transfer

Source Server         : KnowledgeOcean
Source Server Version : 50638
Source Host           : localhost:3306
Source Database       : knowledge_ocean

Target Server Type    : MYSQL
Target Server Version : 50638
File Encoding         : 65001

Date: 2018-05-21 15:37:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `1_bookshelf`
-- ----------------------------
DROP TABLE IF EXISTS `1_bookshelf`;
CREATE TABLE `1_bookshelf` (
  `b_id` int(11) NOT NULL,
  `bs_type` int(11) NOT NULL,
  PRIMARY KEY (`b_id`),
  CONSTRAINT `1_bookshelf_ibfk_1` FOREIGN KEY (`b_id`) REFERENCES `book` (`b_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of 1_bookshelf
-- ----------------------------

-- ----------------------------
-- Table structure for `book`
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `b_id` int(11) NOT NULL AUTO_INCREMENT,
  `b_name` varchar(50) NOT NULL,
  `b_description` varchar(255) NOT NULL,
  `b_author` varchar(50) NOT NULL,
  `bType_id` int(11) NOT NULL,
  `b_cover_url` varchar(255) NOT NULL,
  `b_text_url` varchar(255) NOT NULL,
  `collection_times` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`b_id`),
  KEY `bType_id` (`bType_id`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`bType_id`) REFERENCES `btype` (`bType_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------

-- ----------------------------
-- Table structure for `btype`
-- ----------------------------
DROP TABLE IF EXISTS `btype`;
CREATE TABLE `btype` (
  `bType_id` int(11) NOT NULL,
  `b_type` varchar(50) NOT NULL,
  PRIMARY KEY (`bType_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of btype
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_nickname` varchar(50) NOT NULL,
  `u_name` varchar(50) NOT NULL,
  `u_password` varchar(50) NOT NULL,
  `u_signature` varchar(255) NOT NULL DEFAULT '',
  `u_phone` varchar(50) NOT NULL DEFAULT '',
  `u_email` varchar(50) NOT NULL DEFAULT '',
  `u_purview` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin123', 'admin123', 'root', '', '', '1');
