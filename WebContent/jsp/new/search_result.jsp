<%@page import="com.yzy97.pojo.PagingPojo"%>
<%@page import="com.sun.mail.imap.protocol.Status"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.yzy97.pojo.BookPojo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 排行榜查看书籍显示 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/search_result.js"></script>
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/css/leaderboard_booklist.css" />
</head>
<body>
    <!-- 书籍信息开始 -->
    <c:forEach items="${sessionScope.searchPageInfo.books}" var="book" varStatus="status">
	<div id="content">
	    <div id="book_ranking">
			<span>
			    <h1>
			       <c:out value="${status.index + 1 + (sessionScope.searchPageInfo.currentPage - 1) * 6}"></c:out>
			    </h1>
			</span>
		</div >
		<div id="book_img">
			<img src="${book.bCoveUrl }"></img>
		</div >
		<!-- 图片右侧 -->
		<div id="book_word">
			<div id="book_name">
				<p><font color="red">书名：</font>${book.bName}</p>
			</div>
			<div id="book_type">
			    <p><font color="red">类型：</font>${book.bType}</p>
			</div>
			<div id="book_author">
				<p><font color="red">作者：</font>${book.bAuthor}</p>
			</div>
			<div id="collection_times">
			    <p><font color="red">收藏数：</font>${book.collectionTimes}</p>
			</div>
			<div id="book_description">
				<p><font color="red">简介：</font>${book.bDescription}</p>
			</div>
			<div id="operating">
			    <a href="#" onclick="read('${sessionScope.nickname}','${book.bName}','${book.bAuthor}','1')" >在线阅读</a> 
			    <a href="${book.bTextUrl}" download="">下载</a>
			    <a href="#" onclick="add('${sessionScope.nickname}','${book.bName}','${book.bAuthor}','0')">加入收藏</a>
		    </div>
		</div>
	</div>
	</c:forEach>
	<!-- 书籍信息结束 -->
	
	<!-- 分页显示开始 -->
	<div id="pageContent">
		<p id="pageText">
			共${sessionScope.searchPageInfo.totalData}条数据 &nbsp&nbsp&nbsp&nbsp 总共${sessionScope.searchPageInfo.totalPage}页
			&nbsp&nbsp&nbsp&nbsp
			<button id="previous" onclick="operator('previous','${sessionScope.searchPageInfo.totalPage}','${sessionScope.searchType}','${sessionScope.searchString}')">上一页</button>
			&nbsp&nbsp&nbsp&nbsp 当前第<span id="currentPage" value="${sessionScope.searchPageInfo.currentPage}">${sessionScope.searchPageInfo.currentPage}</span>页
			&nbsp&nbsp&nbsp&nbsp
			<button id="next" onclick="operator('next','${sessionScope.searchPageInfo.totalPage}','${sessionScope.searchType}','${sessionScope.searchString}')">下一页</button>
			&nbsp&nbsp&nbsp&nbsp
			<textarea rows="1" cols="5" id="pageNum" style="resize: none;"
				maxlength="5"></textarea>
			&nbsp&nbsp&nbsp&nbsp
			<button id="go" onclick="operator('go','${sessionScope.searchPageInfo.totalPage}','${sessionScope.searchType}','${sessionScope.searchString}')">go</button>
		</p>
	</div>
	<!-- 分页显示结束 -->
</body>
</html>