<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 注册界面 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>注册</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/register.js"></script>
<script type="text/javascript" >
/*
 * 页面加载时检测后台是否有消息返回
 */
function load(){
	var message = "${information}";
	if(message == ""){
		
	}else{
		alert(message);
	}
	
}
</script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/register.css"></link>
</head>
<body onload="load()">
    <div id="return" align="center" style="font-size:200%;margin-bottom:50px;">
        <a href="${pageContext.request.contextPath}/index.jsp">返回主页</a>
    </div>
	<div id="content" align="center">
			<form id="form" align="left" action="${pageContext.request.contextPath}/register"  method="post">
				<table>
				    <tr>
						<td>昵称</td>
						<td>：</td>
						<td><input type="text" id="nickname" name="nickname" maxlength="12" onblur="nicknameExamine()" onfocus="init('nickname')"/></td>
						<td id="nickname_comment" style="font-size:80%;">请输入2-12个字符的中英文(昵称为你社区显示名称)</td>
					</tr>
					<tr>
						<td>用户名</td>
						<td>：</td>
						<td><input type="text" id="username" name="username" maxlength="16" onblur="usernameExamine()" onfocus="init('username')"/></td>
						<td id="username_comment" style="font-size:80%;">请输入5-16个字符的英文或数字(用户名为你登录账户)</td>
					</tr>
					<tr>
						<td>密码</td>
						<td>：</td>
						<td><input type="password" id="password" name="password" maxlength="16" onblur="passwordExamine()" onfocus="init('password')"/></td>
						<td id="password_comment" style="font-size:80%;">请输入5-16个字符的英文或数字</td>
					</tr>
					<tr>
						<td>确认密码</td>
						<td>：</td>
						<td><input type="password" id="password_t" name="password_t" maxlength="16"  onblur="passwordTExamine()" onfocus="init('password_t')"/></td>
						<td id="password_t_comment" style="font-size:80%;">请与密码相同</td>
					</tr>
					<tr>
						<td>手机</td>
						<td>：</td>
						<td><input type="text" id="phone" name="phone" maxlength="11" onblur="phoneExamine()" onfocus="init('phone')"/></td>
						<td id="phone_comment" style="font-size:80%;">请输入您的11位手机号(可为空)</td>
					</tr>
					<tr>
						<td>E-mail</td>
						<td>：</td>
						<td><input type="text" id="email" name="email" onblur="emailExamine()" onfocus="init('email')"/></td>
						<td id="email_comment" style="font-size:80%;">请输入您的联系邮箱(可为空)</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><input type="button" id="login" value="注册" onclick="register()"/></td>
					</tr>
				</table>
			</form>
	</div>
</body>
</html>