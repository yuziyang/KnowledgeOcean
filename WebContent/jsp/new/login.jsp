<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 登录界面 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登录</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/login.js"></script>
<script type="text/javascript" >
/*
 * 页面加载时检测后台是否有消息返回
 */
function load(){
	usernameExamine();
	passwordExamine();
	var message = "${information}";
	if(message == ""){
		
	}else{
		alert(message);
	}
	
}
</script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css"></link>
</head>
<body onload="load()">
    <div id="return" align="center" style="font-size:200%;margin-bottom:50px;">
        <a href="${pageContext.request.contextPath}/index.jsp">返回主页</a>
    </div>
	<div id="content" align="center">
			<form id="form" align="left" action="${pageContext.request.contextPath}/login" method="post">
				<table>
					<tr >
						<td>用户名</td>
						<td>：</td>
						<td><input type="text" id="username" name="username" onblur="usernameExamine()" onfocus="init('username')"/></td>
					    <td id="username_comment" style="font-size:80%;">请输入5-16个字符的英文或数字(用户名为你登录账户)</td>
					</tr>
					<tr >
						<td>密码</td>
						<td>：</td>
						<td><input type="password" id="password" name="password" onblur="passwordExamine()" onfocus="init('password')"/></td>
						<td id="password_comment" style="font-size:80%;">请输入5-16个字符的英文或数字</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><input type="button" id="login" value="登录" onclick="register()"/></td>
						<td></td>
					</tr>
				</table>
			</form>
	</div>
</body>
</html>