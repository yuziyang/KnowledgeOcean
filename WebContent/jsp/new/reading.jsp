<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TextReader</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/reading.js"></script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/reading.css"></link>
</head>
<body >
	<!--书籍文字界面-->
	<div class='phone'>
		<!--上部-->
		<div class='phone_top'>
			<span class='top_back'></span>
		</div>
		<!--文字内容区-->
		<div class='container'>
		    <!-- 书名 -->
			<h4>${sessionScope.readingPojo.bookname}</h4>
			<!-- 文本内容 -->
			<p>
				${sessionScope.readingPojo.content}
			</p>
		</div>
		<!--下部-->
		
		<div class='phone_bottom'>
		    <button class='pev_page' onclick="pevPage('${sessionScope.readingPojo.bookname}','${sessionScope.readingPojo.fileName}','${sessionScope.readingPojo.type}','${sessionScope.readingPojo.currentPage}')">上一页</button>
			<span class='bottom_back' ></span>
			<button class='next_page' onclick="nextPage('${sessionScope.readingPojo.bookname}','${sessionScope.readingPojo.fileName}','${sessionScope.readingPojo.type}','${sessionScope.readingPojo.currentPage}','${sessionScope.readingPojo.finalyPage}')">下一页</button>
		</div>
	</div>
	<div style="margin-top: 40px; margin-left: 150px; color: white;">
		<p>
			作者：剽悍一小兔 链接：<a href="https://www.jianshu.com/p/0d12a945f0d6">https://www.jianshu.com/p/0d12a945f0d6</a>
			來源：简书 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
		</p>
	</div>
</body>
</html>