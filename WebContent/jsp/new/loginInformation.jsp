<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 登录结果页面 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script>
     function load(){
    	window.location = "${pageContext.request.contextPath}/index.jsp"
    }
     window.onload =window.setTimeout("load()", 5000);
</script>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/css/registerInformation.css"></link>
<title>登录成功，正在跳转...</title>
</head>
<body align="center">
    <p>登录成功，正在跳转到首页...</p><br/>
    <a href="${pageContext.request.contextPath}/index.jsp">去首页</a>
</body>
</html>