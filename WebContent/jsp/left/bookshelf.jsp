<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!--书架左部 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	overflow-x: hidden;
	overflow-y: hidden;
	background-color: #99D3AD;
}

li {
	font-size: 150%;
	padding: 10px;
}

li:hover {
	color: red;
	box-shadow: 0px 1px 3px red;
}

a {
	text-decoration: none;
	list-style-image: url(../../imag/sword.jpg);
}
</style>
<script>
    //选中菜单项之后高亮显示
	var div;
	//书架查看
	function forwordBookshelf(id,bsType,nickname) {
		// 高亮显示选中菜单
		if (div != null) {
			div.style.backgroundColor = "#99D3AD";
			div = document.getElementById(id);
			div.style.backgroundColor = "blue";
			request = "/KnowledgeOcean/bookshelf?currentPage=1&nickname=" + nickname + "&bsType=" + bsType;
			var xhr = new XMLHttpRequest();
			xhr.open("GET", request);
			xhr.send(null);
			xhr.onreadystatechange = function(){
	    		window.open("/KnowledgeOcean/jsp/right/bookshelf_" +  id + ".jsp","right");
	    	}
		} else if (div == null) {
			div = document.getElementById(id);
			div.style.backgroundColor = "blue";
			request = "/KnowledgeOcean/bookshelf?currentPage=1&nickname=" + nickname + "&bsType=" + bsType;
			var xhr = new XMLHttpRequest();
			xhr.open("GET", request);
			xhr.send(null);
			xhr.onreadystatechange = function(){
	    		window.open("/KnowledgeOcean/jsp/right/bookshelf_" +  id + ".jsp","right");
	    	}	
		}

	}
</script>
</head>
<body>
	<div id="navigation">
		<ul>
			<a href="#" target="right" onClick="forwordBookshelf('reading','1','${sessionScope.nickname}')"><li
				id="reading">正在阅读</li></a>
			<a href="#" target="right" onClick="forwordBookshelf('collection','0','${sessionScope.nickname}')"><li
				id="collection">收藏</li></a>
		</ul>
	</div>
</body>
</html>