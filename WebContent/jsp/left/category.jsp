<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!--分类查看的左部  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body {
	overflow-x: hidden;
	overflow-y: hidden;
	background-color: #99D3AD;
}

li {
	font-size: 150%;
	padding: 10px;
}

li:hover {
	color: red;
	box-shadow: 0px 1px 3px red;
}

a {
	text-decoration: none;
}

#xianxia {
	list-style-image: url(../../imag/xianxia.jpg);
}

#magic {
	list-style-image: url(../../imag/magic.jpg);
}

#city {
	list-style-image: url(../../imag/city.jpg);
}

#history {
	list-style-image: url(../../imag/history.jpg);
}

#science {
	list-style-image: url(../../imag/science.jpg);
}

#literature {
	list-style-image: url(../../imag/literature.jpg);
}
</style>
<script>
    //选中菜单项之后高亮显示
	var div;
	function forword(id) {
		if (div != null) {
			div.style.backgroundColor = "#99D3AD";
			div = document.getElementById(id);
			div.style.backgroundColor = "blue";
			request = "/KnowledgeOcean/categoryBooklist" + "?type=" + id +"&currentPage=1";
			var xhr = new XMLHttpRequest();
			xhr.open("GET", request);
			xhr.send(null);
			xhr.onreadystatechange = function(){
        		window.location.reload(true);
        		window.open("/KnowledgeOcean/jsp/right/category_booklist.jsp","right");
        	}
		} else if (div == null) {
			div = document.getElementById(id);
			div.style.backgroundColor = "blue";
			request = "/KnowledgeOcean/categoryBooklist" + "?type=" + id +"&currentPage=1";
			var xhr = new XMLHttpRequest();
			xhr.open("GET", request);
			xhr.send(null);
			xhr.onreadystatechange = function(){
        		window.location.reload(true);
        		window.open("/KnowledgeOcean/jsp/right/category_booklist.jsp","right");
        	}
		}
	}
</script>
</head>
<body>
	<div id="navigation">
		<ul>
			<a  href="#" target="right" onclick="forword('xianxia')">
			    <li id="xianxia">仙侠</li>
			</a>
			<a  href="#" target="right" onclick="forword('magic')">
			    <li id="magic">魔幻</li>
			</a>
			<a  href="#" target="right" onclick="forword('city')">
			    <li id="city">都市</li>
		    </a>
			<a  href="#" target="right" onclick="forword('history')">
			    <li id="history">历史</li>
			</a>
			<a  href="#" target="right" onclick="forword('science')">
			    <li id="science">科幻</li>
			</a>
			<a  href="#" target="right" onclick="forword('literature')">
			    <li id="literature">文学</li>
			</a>
		</ul>
	</div>
</body>
</html>