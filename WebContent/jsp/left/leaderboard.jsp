<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 排行榜查看的左部 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style>
    body{
        overflow-x:hidden;
        overflow-y:hidden;
        background-color:#99D3AD;
    }
    li{
        font-size:150%;
        padding:10px;
    }
    li:hover{
        color:red;
        box-shadow:0px 1px 3px red;
    }
    a{
        text-decoration:none;
        list-style-image:url(../../imag/sword.jpg);
    }
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/leaderboard.js"></script>
</head>
<body>
    <div id="navigation">
        <ul>
            <a href="../right/leaderboard_booklist.jsp" target="right" onClick="forwordLeaderboard('/KnowledgeOcean/leaderboardBooklist','overall_ranking')"><li id="overall_ranking">总排行榜</li></a>
        </ul>
    </div>
</body>
</html>