<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 个人设置界面左部 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
    body{
        background-color:#99D3AD;
    }
    #content{
        margin-top:20px;
    }
    #admin{
        visibility:hidden;
    }
    
    a{
        text-decoration:none;
        color:black;
        list-style-image:url(../../imag/sword.jpg);
        font-size:170%;
    }
    a:hover{
        color:red;
        box-shadow:0px 1px 3px red;
    }
</style>
<script type="text/javascript">
    //选中菜单项之后高亮显示
	var div;
	function forword(id) {
		if (div != null) {
			div.style.backgroundColor = "#99D3AD";
			div = document.getElementById(id);
			div.style.backgroundColor = "blue";
		} else if (div == null) {
			div = document.getElementById(id);
			div.style.backgroundColor = "blue";
		}
	}
	//根据权限显示菜单项
	function load(){
		var purview = "${sessionScope.purview}";
		if(purview == "1"){
			document.getElementById("admin").style.visibility = "visible";
		}
	}
</script>
</head>
<body onload="load()">
    <div id="content">
        <div id="user">
            <ul>
                <a href="${pageContext.request.contextPath}/jsp/right/personal_information.jsp" target="right" onClick="forword('personal_information')"><li id="personal_information">个人资料</li></a>
            </ul>
        </div>
        <div id="admin">
            <ul>
                <a href="${pageContext.request.contextPath}/jsp/right/book_on_shelv.jsp" target="right" onClick="forword('book_on_shelv')"><li id="book_on_shelv">上架书籍</li></a>
            </ul>
        </div>
    </div>
</body>
</html>