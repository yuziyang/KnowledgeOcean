<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.yzy97.pojo.BookPojo" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- 分类查看书籍显示 -->
<!-- 每排四个 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/css/category_booklist.css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/bookshelf.js"></script>
<script type="text/javascript">
    function load(){
    	var message = "${information}";
    	if(message == ""){
    		
    	}else{
    		alert(message);
    	}
    }
</script>
</head>
<!-- ${book.bCoveUrl } -->
<body onload="load()">
	<c:forEach items="${sessionScope.bookshelfPagInfo.books}" var="book">
		<!-- 书籍显示框开始 -->
		<div id="bookContent">
			<div id="book_img">
				<img src="${book.bCoveUrl }"></img>
			</div>
			<!-- 图片右侧 -->
			<div id="book_word">
				<div id="book_name">
					<p>
						<font color="red">书名：</font>${book.bName}
					</p>
				</div>
				<div id="book_author">
					<p>
						<font color="red">作者：</font>${book.bAuthor}
					</p>
				</div>
				<div id="book_type">
					<p>
						<font color="red">类型：</font>${book.bType}
					</p>
				</div>
				<div id="book_description">
					<p>
						<font color="red">简介：</font><span>${book.bDescription}</span>
					</p>
				</div>
				<div id="operating">
					<a href="#" onclick="read('${sessionScope.nickname}','${book.bName}','${book.bAuthor}','1')" >在线阅读</a> 
					<a href="${book.bTextUrl}" download="">下载</a> 
				</div>
			</div>
		</div>
		<!-- 书籍显示框结束 -->
	</c:forEach>
	<!-- 分页显示开始 -->
	<div id="pageContent">
		<p id="pageText">
			共${sessionScope.bookshelfPagInfo.totalData}条数据 &nbsp&nbsp&nbsp&nbsp 总共${sessionScope.bookshelfPagInfo.totalPage}页
			&nbsp&nbsp&nbsp&nbsp
			<button id="previous" onclick="operator('previous','collection','${sessionScope.bookshelfPagInfo.totalPage}','${sessionScope.nickname}','0')">上一页</button>
			&nbsp&nbsp&nbsp&nbsp 当前第<span id="currentPage" value="${sessionScope.bookshelfPagInfo.currentPage}">${sessionScope.bookshelfPagInfo.currentPage}</span>页
			&nbsp&nbsp&nbsp&nbsp
			<button id="next" onclick="operator('next','collection','${sessionScope.bookshelfPagInfo.totalPage}','${sessionScope.nickname}','0')">下一页</button>
			&nbsp&nbsp&nbsp&nbsp
			<textarea rows="1" cols="5" id="pageNum" style="resize: none;"
				maxlength="5"></textarea>
			&nbsp&nbsp&nbsp&nbsp
			<button id="go" onclick="operator('go','collection','${sessionScope.bookshelfPagInfo.totalPage}','${sessionScope.nickname}','0')">go</button>
		</p>
	</div>
	<!-- 分页显示结束 -->
</body>
</html>