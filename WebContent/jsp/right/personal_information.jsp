<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- 个人资料右部 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="javascript" src="/KnowledgeOcean/js/personal_information.js"></script>
<script language="javascript" >
    function load(){
    	
    	document.getElementById("text_nickname").disabled = true;
    	document.getElementById("text_signature").disabled = true;
    	document.getElementById("text_phone").disabled = true;
    	document.getElementById("text_email").disabled = true;
    	
    	var message = "${information}";
    	if(message == ""){    		
    	}else{
    		alert(message);
    	}
    	
    }
</script>
</head>
<body onload="load()">
    <table align="center">
        <tr>
            <td>昵称</td>
            <td>：</td>
            <td><input id="text_nickname" type="text" value="${sessionScope.nickname}" readonly="ture" maxlength="12" onblur="nicknameExamine()" onfocus="init('text_nickname','${sessionScope.nickname}')"/></td>
            <td style="width:300px;"><span id="nickname_comment" style="font-size:80%;"></span></td>
        </tr>
        <tr>
            <td>个性签名</td>
            <td>：</td>
            <td><input id="text_signature" type="text" value="${sessionScope.signature}" readonly="ture" maxlength="255"/></td>
            <td style="width:300px;"><span id="signature_comment" style="font-size:80%;"></span></td>
        </tr>
        <tr>
            <td>手机</td>
            <td>：</td>
            <td><input id="text_phone" type="text" value="${sessionScope.phone}" readonly="ture" maxlength="11" onblur="phoneExamine()" onfocus="init('text_phone','${sessionScope.phone}')"/></td>
            <td style="width:300px;"><span id="phone_comment" style="font-size:80%;"></span></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td>：</td>
            <td><input id="text_email" type="text" value="${sessionScope.email}" readonly="ture" onblur="emailExamine()" onfocus="init('text_email','${sessionScope.email}')" maxlength="30"/></td>
            <td style="width:300px;"><span id="email_comment" style="font-size:80%;"></span></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="button" id="operator" value="编辑" onClick="operator()" /></td>
            <td></td>
            <td></td>
        </tr>
        
    </table>
</body>
</html>