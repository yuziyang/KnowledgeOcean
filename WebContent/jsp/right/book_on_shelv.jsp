<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- 上架书籍的界面 -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
#f_img {
	width: 50px;
	height: 20px;
	border: 2px solid black;
	background-color: #99D3AD;
}

#add_img {
	width: 0px;
	height: 0px;
}
#f_book {
	width: 50px;
	height: 20px;
	border: 2px solid black;
	background-color: #99D3AD;
}

#add_book {
	width: 0px;
	height: 0px;
	margin-top:10px;
	margin-bottom:30px;
}
</style>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/book_on_shelf.js"></script>
<script type="text/javascript" >
/*
 * 页面加载时检测后台是否有消息返回
 */
function load(){
	var message = "${information}";
	if(message == ""){
		
	}else{
		alert(message);
	}
	
}
</script>
</head>
<body onload="load()">
	<div id="content" align="center">
		<form id="form" enctype="multipart/form-data" method="post" action="${pageContext.request.contextPath}/bookOnShelf">
			<label for="add_img" value="添加书籍封面" id="f_img"  onChange="previewFile()">点击添加书籍封面(大小为100px*150px)<input
				type="file" id="add_img" name="add_img" style="opacity: 0;" accept="image/*" /></label><br />
			<img id="picture" name="picture" src="" height="150" width="100" alt="Image preview..." style="margin-top:10px;margin-bottom:10px;"/><br />
			<input type="text" id="img_name" name="img_name" value="" style="border:none;margin-left:120px;" readonly="true"/><br/>
			书名：<input type="text" id="bookname" name="bookname"/><br /><br />
			简介：<input type="text" id="description" name="description"/><br /><br />
			作者：<input type="text" id="authorname" name="authorname"/><br /><br /> 
			类型：<select id="type" name="type">
				<option value="xianxia">仙侠</option>
				<option value="magic">魔幻</option>
				<option value="city">都市</option>
				<option value="history">历史</option>
				<option value="science">科幻</option>
				<option value="literature">文学</option>
			</select><br /> 
			<label for="add_book" value="添加书籍文件" id="f_book" onChange="displayName()">点击添加书籍文件<input
				type="file" id="add_book" name="add_book" style="opacity: 0;" accept="text/plain" /></label><br />
				<input type="text" id="book_name" name="book_name" value="" style="border:none;margin-left:80px;" readonly="true"/><br/><br/>
				<input type="button"  value="提交" onclick="submitForm()"/>
		</form>
	</div>
</body>
</html>