<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width,initial-scale=1"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/top.css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/top.js"></script>
<script type="text/javascript">
    function load(){
        var nicknameStr = "${sessionScope.nickname}";
        var nickname = document.getElementById("nickname");
        if(nicknameStr == ""){
        	nickname.innerHTML = "请登录";
        }else{
        	nickname.innerHTML = "欢迎您," + nicknameStr;
        }
    }
</script>
</head>
<body onload="load()">
<!-- 内容开始 -->
<div id="content">

    <!-- 标志开始 -->
    <div id="logo">
        <img src="${pageContext.request.contextPath}/imag/logo.png" ></img>
    </div>
    <!-- 标志结束 -->
    
    <!-- 导航开始 -->
    <div id="navigation" >
        <div id="category">
            <a href="left/category.jsp" target="left" onclick="forwordCategory('/KnowledgeOcean/categoryBooklist','category')">分类查看</a>
        </div>
        <div id="leaderboard">
            <a href="left/leaderboard.jsp" target="left" onclick="forwordLeaderboard('/KnowledgeOcean/leaderboardBooklist','leaderboard')">排行榜查看</a>
        </div>
        <div id="borrow_records">
            <a href="left/bookshelf.jsp" target="left" onclick="forwordBookshelf('/KnowledgeOcean/bookshelf','borrow_records','${sessionScope.nickname}')">书架</a>
        </div>
        <!-- 搜索框开始 -->
        <div id="search">
                <input type="text" style="float:left;" id="search_string"/>
                <select style="float:left;" id="search_type">
                    <option value="author">作者</option>
                    <option value="bookname">书名</option>
                </select>
                <input type="submit" value="搜索" id="search_button" onclick="search()"/>
        </div>
        <!-- 搜索框结束 -->
        <div id="user">
            <a id="nickname" href="#" target="left" onclick="request('${sessionScope.nickname}','${pageContext.request.contextPath}/jsp/right/personal_information.jsp','user')"></a>
        </div>
        <!--登录/注册开始  -->
        <div id="login_register">
            <a><input type="button" value="登录" onClick="login('new/login.jsp')"/></a>
            <a><input type="button" value="注册" onClick="login('new/register.jsp')"/></a>
        </div>
        <!--登录/注册结束  -->
    </div>
    <!-- 导航结束 -->
    
</div>
<!-- 内容结束 -->
</body>
</html>